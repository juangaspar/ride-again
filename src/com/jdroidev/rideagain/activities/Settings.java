package com.jdroidev.rideagain.activities;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.apache.http.Header;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.countrypicker.CountryPicker;
import com.countrypicker.CountryPickerListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.jdroidev.rideagain.R;
import com.jdroidev.rideagain.api.ApiConnector;
import com.jdroidev.rideagain.api.SynchronizationResult;
import com.jdroidev.rideagain.game.BikeStepsPackage;
import com.jdroidev.rideagain.game.Level;
import com.jdroidev.rideagain.game.Rider;
import com.jdroidev.rideagain.game.SharedPreferencesStorage;
import com.jdroidev.rideagain.game.Track;
import com.jdroidev.rideagain.game.Wheel;
import com.jdroidev.rideagain.game.trackelements.TrackRecord;
import com.jdroidev.rideagain.tracks.LevelsDefinition;
import com.jdroidev.rideagain.ui.WheelModelPicker;
import com.jdroidev.rideagain.ui.WheelPickerListener;
import com.jdroidev.rideagain.utils.Utils;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class Settings extends FragmentActivity {

  private Boolean audioEnabled;
  private Dialog riderSettingsDialog;

  private Rider rider;
  private ProgressDialog pd;
  private Boolean ghostGlobalModeEnabled;
  private Boolean ghostLocalModeEnabled;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_settings);

    rider = SharedPreferencesStorage.get("rider", Rider.class,
        getApplicationContext());
    if (rider == null) {
      rider = new Rider("", "es", 1);
    }

    setUpUi();

    // Look up the AdView as a resource and load a request.
    AdView adView = (AdView) this.findViewById(R.id.adView);
    if (Utils.isFreeVersion) {
      AdRequest adRequest = new AdRequest.Builder().addTestDevice(
          "33B892C281D109B2").build();
      adView.loadAd(adRequest);
    } else {
      adView.setVisibility(View.GONE);
    }
  }

  private void setUpUi() {

    Button riderSettingsButton = (Button) findViewById(R.id.riderSettingsButton);
    riderSettingsButton.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        runOnUiThread(new Runnable() {

          @Override
          public void run() {
            openRiderSettingsDialog();
          }
        });
      }
    });

    ImageView backButton = (ImageView) findViewById(R.id.backButton);
    backButton.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        Intent intent = new Intent(getApplicationContext(), Main.class);
        startActivity(intent);
        finish();
      }
    });

    // set audio settings
    audioEnabled = SharedPreferencesStorage.get("audioEnabled", Boolean.class,
        getApplicationContext());
    Button toogleSoundsButton = (Button) findViewById(R.id.toogleSoundsButton);
    Drawable iconLeft = getResources().getDrawable(R.drawable.speaker_32);
    iconLeft.setBounds(0, 0, 32, 32);
    if (audioEnabled == null || audioEnabled == false) {
      Drawable icon = getResources().getDrawable(R.drawable.red_cross_30);
      icon.setBounds(0, 0, 32, 32);
      toogleSoundsButton.setCompoundDrawables(iconLeft, null, icon, null);
    } else {
      Drawable icon = getResources().getDrawable(R.drawable.tick_32);
      icon.setBounds(0, 0, 32, 32);
      toogleSoundsButton.setCompoundDrawables(iconLeft, null, icon, null);
    }
    toogleSoundsButton.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        Button toogleSoundsButton = (Button) findViewById(R.id.toogleSoundsButton);
        Drawable iconLeft = getResources().getDrawable(R.drawable.speaker_32);
        iconLeft.setBounds(0, 0, 32, 32);
        if (audioEnabled == null || audioEnabled == false) {
          audioEnabled = true;
          SharedPreferencesStorage.save(audioEnabled, "audioEnabled",
              getApplicationContext());
          Drawable icon = getResources().getDrawable(R.drawable.tick_32);
          icon.setBounds(0, 0, 32, 32);
          toogleSoundsButton.setCompoundDrawables(iconLeft, null, icon, null);
        } else {
          audioEnabled = false;
          SharedPreferencesStorage.save(audioEnabled, "audioEnabled",
              getApplicationContext());
          Drawable icon = getResources().getDrawable(R.drawable.red_cross_30);
          icon.setBounds(0, 0, 32, 32);
          toogleSoundsButton.setCompoundDrawables(iconLeft, null, icon, null);
        }
      }
    });

    // set ghost mode settings
    ghostGlobalModeEnabled = SharedPreferencesStorage.get(
        "ghostGlobalModeEnabled", Boolean.class, getApplicationContext());
    ghostGlobalModeEnabled = (ghostGlobalModeEnabled == null) ? false
        : ghostGlobalModeEnabled;

    ghostLocalModeEnabled = SharedPreferencesStorage.get(
        "ghostLocalModeEnabled", Boolean.class, getApplicationContext());
    ghostLocalModeEnabled = (ghostLocalModeEnabled == null) ? false
        : ghostLocalModeEnabled;

    Button ghostModeButton = (Button) findViewById(R.id.ghostModeButton);
    iconLeft = getResources().getDrawable(R.drawable.ghost_wheel);
    iconLeft.setBounds(0, 0, 42, 32);
    Drawable icon;
    if (ghostGlobalModeEnabled == false && ghostLocalModeEnabled == false) {
      icon = getResources().getDrawable(R.drawable.red_cross_30);
      icon.setBounds(0, 0, 32, 32);
    } else if (ghostGlobalModeEnabled == true && ghostLocalModeEnabled == false) {
      icon = getResources().getDrawable(R.drawable.world_32);
      icon.setBounds(0, 0, 32, 32);
    } else if (ghostGlobalModeEnabled == false && ghostLocalModeEnabled == true) {
      icon = getResources().getDrawable(R.drawable.mobile_32);
      icon.setBounds(0, 0, 32, 32);
    } else {
      icon = getResources().getDrawable(R.drawable.world_mobile_32);
      icon.setBounds(0, 0, 32, 32);
    }

    ghostModeButton.setCompoundDrawables(iconLeft, null, icon, null);
    ghostModeButton.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {

        if (ghostGlobalModeEnabled == false && ghostLocalModeEnabled == false) {
          ghostLocalModeEnabled = true;
        } else if (ghostGlobalModeEnabled == false
            && ghostLocalModeEnabled == true) {
          ghostGlobalModeEnabled = true;
          ghostLocalModeEnabled = false;
        } else if (ghostGlobalModeEnabled == true
            && ghostLocalModeEnabled == false) {
          ghostGlobalModeEnabled = true;
          ghostLocalModeEnabled = true;
        } else {
          ghostGlobalModeEnabled = false;
          ghostLocalModeEnabled = false;
        }

        SharedPreferencesStorage.save(ghostLocalModeEnabled,
            "ghostLocalModeEnabled", getApplicationContext());
        SharedPreferencesStorage.save(ghostGlobalModeEnabled,
            "ghostGlobalModeEnabled", getApplicationContext());

        Button ghostModeButton = (Button) findViewById(R.id.ghostModeButton);
        Drawable iconLeft = getResources().getDrawable(R.drawable.ghost_wheel);
        iconLeft.setBounds(0, 0, 42, 32);
        Drawable icon;
        if (ghostGlobalModeEnabled == false && ghostLocalModeEnabled == false) {
          icon = getResources().getDrawable(R.drawable.red_cross_30);
          icon.setBounds(0, 0, 32, 32);
        } else if (ghostGlobalModeEnabled == true
            && ghostLocalModeEnabled == false) {
          icon = getResources().getDrawable(R.drawable.world_32);
          icon.setBounds(0, 0, 32, 32);
        } else if (ghostGlobalModeEnabled == false
            && ghostLocalModeEnabled == true) {
          icon = getResources().getDrawable(R.drawable.mobile_32);
          icon.setBounds(0, 0, 32, 32);
        } else {
          icon = getResources().getDrawable(R.drawable.world_mobile_32);
          icon.setBounds(0, 0, 32, 32);
        }

        ghostModeButton.setCompoundDrawables(iconLeft, null, icon, null);

      }
    });

    Button synchronizeButton = (Button) findViewById(R.id.synchronizeButton);
    synchronizeButton.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        // show dialog
        pd = ProgressDialog.show(Settings.this, null,
            getString(R.string.synchronizing_data), true, false);

        (new Thread(new Runnable() {

          @Override
          public void run() {
            synchronizeResults();
          }
        })).start();

      }
    });

    Long lastSynchronization = SharedPreferencesStorage.get(
        "lastSynchronization", long.class, getApplicationContext());

    if (lastSynchronization != null) {
      Date date = new Date(lastSynchronization);
      java.text.DateFormat dateFormat = android.text.format.DateFormat
          .getDateFormat(getApplicationContext());
      java.text.DateFormat timeFormat = android.text.format.DateFormat
          .getTimeFormat(getApplicationContext());

      TextView lastSynchronizationTextView = (TextView) findViewById(R.id.lastShyncronizationDate);
      lastSynchronizationTextView.setText(dateFormat.format(date) + " "
          + timeFormat.format(date));
    }

  }

  protected void openRiderSettingsDialog() {
    riderSettingsDialog = new Dialog(Settings.this);
    riderSettingsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    riderSettingsDialog.setContentView(R.layout.dialog_rider_settings);
    riderSettingsDialog.setCanceledOnTouchOutside(false);
    riderSettingsDialog.setCancelable(false);

    Button saveRiderSettingsButton = (Button) riderSettingsDialog
        .findViewById(R.id.saveRiderSettingsButton);
    saveRiderSettingsButton.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        EditText riderName = (EditText) riderSettingsDialog
            .findViewById(R.id.riderNameField);
        rider.name = riderName.getText().toString();

        SharedPreferencesStorage.save(rider, "rider", getApplicationContext());

        riderSettingsDialog.dismiss();

      }
    });

    Button cancelRiderSettingsButton = (Button) riderSettingsDialog
        .findViewById(R.id.cancelRiderSettingsButton);
    cancelRiderSettingsButton.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {

        riderSettingsDialog.dismiss();

      }
    });

    EditText riderNameField = (EditText) riderSettingsDialog
        .findViewById(R.id.riderNameField);
    riderNameField.setText(rider.name);

    ImageView riderFlagImage = (ImageView) riderSettingsDialog
        .findViewById(R.id.riderFlag);
    String drawableName = "flag_" + rider.flag;
    riderFlagImage.setImageResource(getResId(drawableName));
    riderFlagImage.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        final CountryPicker picker = CountryPicker.newInstance(getResources()
            .getString(R.string.flag));
        picker.setListener(new CountryPickerListener() {

          @Override
          public void onSelectCountry(String name, String code) {
            ImageView riderFlagImage = (ImageView) riderSettingsDialog
                .findViewById(R.id.riderFlag);
            // Load drawable dynamically from country code
            String drawableName = "flag_" + code.toLowerCase(Locale.ENGLISH);
            riderFlagImage.setImageResource(getResId(drawableName));

            rider.flag = code.toLowerCase(Locale.ENGLISH);
            picker.dismiss();
          }
        });

        picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
      }

    });

    Wheel wheel = Wheel.load(rider.wheelId);

    ImageView riderWheelImage = (ImageView) riderSettingsDialog
        .findViewById(R.id.riderWheelModel);
    riderWheelImage.setImageResource(getResources().getIdentifier(wheel.design,
        "drawable", getPackageName()));
    riderWheelImage.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        final WheelModelPicker picker = WheelModelPicker
            .newInstance(getResources().getString(R.string.design));
        picker.setListener(new WheelPickerListener() {

          @Override
          public void onSelectWheel(Integer id) {
            ImageView riderWheelModel = (ImageView) riderSettingsDialog
                .findViewById(R.id.riderWheelModel);
            Wheel wheel = Wheel.load(id);
            riderWheelModel.setImageResource(getResources().getIdentifier(
                wheel.design, "drawable", getPackageName()));
            rider.wheelId = id;
            picker.dismiss();

          }
        });

        picker.show(getSupportFragmentManager(), "WHEEL_PICKER");
      }

    });

    riderSettingsDialog.show();

  }

  private void synchronizeResults() {

    LevelsDefinition levels = new LevelsDefinition();
    ArrayList<TrackRecord> records = new ArrayList<TrackRecord>();

    for (Level level : levels.levels) {
      for (Track track : level.tracks) {
        TrackRecord trackRecord = SharedPreferencesStorage.get("trackRecord_"
            + track.id + "_" + level.mode, TrackRecord.class,
            getApplicationContext());

        if (trackRecord == null || trackRecord.isSynchronized)
          continue;

        trackRecord.b = SharedPreferencesStorage.get("trackRecord_" + track.id
            + "_" + level.mode + "_steps", BikeStepsPackage.class,
            getApplicationContext());

        records.add(trackRecord);
      }
    }

    ApiConnector.post("synchronizeResults", new AsyncHttpResponseHandler() {
      @Override
      public void onSuccess(String response) {

        SynchronizationResult result = SharedPreferencesStorage.JsonToObject(
            response, SynchronizationResult.class);

        if (result != null) {
          for (TrackRecord trackRecord : result.trackRecords) {
            SharedPreferencesStorage.save(trackRecord.b, "globalTrackRecord_"
                + trackRecord.i + "_" + trackRecord.m + "_steps",
                getApplicationContext());

            trackRecord.b = null;
            trackRecord.isSynchronized = true;
            SharedPreferencesStorage.save(trackRecord, "globalTrackRecord_"
                + trackRecord.i + "_" + trackRecord.m, getApplicationContext());
          }
        }

        SharedPreferencesStorage.save(System.currentTimeMillis(),
            "lastSynchronization", getApplicationContext());

        runOnUiThread(new Runnable() {

          @Override
          public void run() {
            Date date = new Date(System.currentTimeMillis());
            java.text.DateFormat dateFormat = android.text.format.DateFormat
                .getDateFormat(getApplicationContext());
            java.text.DateFormat timeFormat = android.text.format.DateFormat
                .getTimeFormat(getApplicationContext());

            TextView lastSynchronizationTextView = (TextView) findViewById(R.id.lastShyncronizationDate);
            lastSynchronizationTextView.setText(dateFormat.format(date) + " "
                + timeFormat.format(date));

            pd.dismiss();

          }
        });

      }

      @Override
      public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
        pd.dismiss();
      }
    }, SharedPreferencesStorage.objectToJson(records));

  }

  @Override
  public void onBackPressed() {
    Intent intent = new Intent(getApplicationContext(), Main.class);
    startActivity(intent);
    finish();

  }

  private int getResId(String drawableName) {

    try {
      Class<com.jdroidev.rideagain.R.drawable> res = R.drawable.class;
      Field field = res.getField(drawableName);
      int drawableId = field.getInt(null);
      return drawableId;
    } catch (Exception e) {
      Log.e("COUNTRYPICKER", "Failure to get drawable id.", e);
    }
    return -1;
  }
}
