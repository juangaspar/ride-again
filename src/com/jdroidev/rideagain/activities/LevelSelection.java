package com.jdroidev.rideagain.activities;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.jdroidev.rideagain.R;
import com.jdroidev.rideagain.game.Level;
import com.jdroidev.rideagain.game.SharedPreferencesStorage;
import com.jdroidev.rideagain.tracks.LevelsDefinition;
import com.jdroidev.rideagain.utils.Utils;

public class LevelSelection extends FragmentActivity {

	LevelsPagerAdapter levelsPagerAdapter;
	ViewPager viewPager;
	protected int selectedLevelId;
	private static Integer totalLightnings;
	private static ArrayList<Level> levels;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_level_selection);
		loadLevels();
		setUpUi();

		// Look up the AdView as a resource and load a request.
		AdView adView = (AdView) this.findViewById(R.id.adView);
		if (Utils.isFreeVersion) {
			AdRequest adRequest = new AdRequest.Builder().addTestDevice(
					"33B892C281D109B2").build();
			adView.loadAd(adRequest);
		} else {
			adView.setVisibility(View.GONE);
		}
	}

	private void setUpUi() {
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the activity.
		levelsPagerAdapter = new LevelsPagerAdapter(getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		viewPager = (ViewPager) findViewById(R.id.levelsPager);
		viewPager.setAdapter(levelsPagerAdapter);

		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				selectedLevelId = levels.get(arg0).id;

				ImageView leftArrow = (ImageView) findViewById(R.id.leftArrow);
				ImageView rightArrow = (ImageView) findViewById(R.id.rightArrow);

				if (arg0 == 0) {
					leftArrow.setVisibility(View.INVISIBLE);
				} else {
					leftArrow.setVisibility(View.VISIBLE);
				}

				if (arg0 < (levels.size() - 1)) {
					rightArrow.setVisibility(View.VISIBLE);
				} else {
					rightArrow.setVisibility(View.INVISIBLE);
				}

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});

		// set selected level id
		for (int i = 0; i < levels.size(); i++) {
			if (levels.get(i).id == selectedLevelId) {
				viewPager.setCurrentItem(i);
			}
		}

		ImageView backButton = (ImageView) findViewById(R.id.backButton);
		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), Main.class);
				startActivity(intent);
				finish();
			}
		});

		ImageView leftArrow = (ImageView) findViewById(R.id.leftArrow);
		leftArrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
			}
		});

		ImageView rightArrow = (ImageView) findViewById(R.id.rightArrow);
		rightArrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
			}
		});

	}

	private void loadLevels() {
		LevelsDefinition levelsDefinition = new LevelsDefinition();
		levels = levelsDefinition.levels;

		// set selected level
		int levelId = getIntent().getIntExtra("levelId", 1);
		selectedLevelId = levelId;

		totalLightnings = SharedPreferencesStorage.get("totalLightnings",
				Integer.class, getApplicationContext());
		totalLightnings = (totalLightnings == null) ? 0 : totalLightnings;

		ImageView leftArrow = (ImageView) findViewById(R.id.leftArrow);
		leftArrow.setVisibility(View.INVISIBLE);
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class LevelsPagerAdapter extends FragmentPagerAdapter {

		public LevelsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a PlaceholderFragment (defined as a static inner class
			// below).
			return PlaceholderFragment.newInstance(position);
		}

		@Override
		public int getCount() {
			return levels.size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return (position + 1) + ". "
					+ getResources().getString(levels.get(0).nameResource);
		}

	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_LEVEL_POSITION = "level_position";

		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static PlaceholderFragment newInstance(int position) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_LEVEL_POSITION, position);
			fragment.setArguments(args);
			return fragment;
		}

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_level_selection,
					container, false);
			int levelPosition = getArguments().getInt(ARG_LEVEL_POSITION);
			final Level level = levels.get(levelPosition);
			Integer currentLightnings = SharedPreferencesStorage.get(
					"levelLightnings_" + level.id, Integer.class, getActivity()
							.getApplicationContext());

			currentLightnings = (currentLightnings == null) ? 0
					: currentLightnings;

			// set level name
			TextView levelTitle = (TextView) rootView
					.findViewById(R.id.levelTitle);
			levelTitle.setText((levelPosition + 1) + ". "
					+ getResources().getString(level.nameResource));

			if (totalLightnings >= level.unlockLightnings) {
				View lockedLevelView = rootView
						.findViewById(R.id.levelImageLayer);
				lockedLevelView.setVisibility(View.GONE);

				// set level lightnings
				TextView levelLightnings = (TextView) rootView
						.findViewById(R.id.levelLightnings);
				levelLightnings.setText(currentLightnings + " "
						+ getResources().getString(R.string.of) + " "
						+ level.totalLightnings);

				View goToTrackSelectionButton = (View) rootView
						.findViewById(R.id.levelImage);
				goToTrackSelectionButton
						.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								Intent intent = new Intent(getActivity()
										.getApplicationContext(),
										TrackSelection.class);
								intent.putExtra("levelId", level.id);
								startActivity(intent);
								getActivity().finish();

							}
						});
			} else {
				TextView levelLightnings = (TextView) rootView
						.findViewById(R.id.levelLightnings);
				levelLightnings.setVisibility(View.INVISIBLE);
				ImageView lightningImage = (ImageView) rootView
						.findViewById(R.id.lightningImage);
				lightningImage.setVisibility(View.INVISIBLE);

				TextView unlockLevelText1 = (TextView) rootView
						.findViewById(R.id.unlockLevelText1);
				unlockLevelText1
						.setText((level.unlockLightnings - totalLightnings)
								+ "");

			}

			return rootView;
		}
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(getApplicationContext(), Main.class);
		startActivity(intent);
		finish();
	}
}
