package com.jdroidev.rideagain.activities;

import java.lang.reflect.Field;
import java.util.Locale;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.countrypicker.CountryPicker;
import com.countrypicker.CountryPickerListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.jdroidev.rideagain.R;
import com.jdroidev.rideagain.game.Rider;
import com.jdroidev.rideagain.game.SharedPreferencesStorage;
import com.jdroidev.rideagain.game.Wheel;
import com.jdroidev.rideagain.ui.WheelModelPicker;
import com.jdroidev.rideagain.ui.WheelPickerListener;
import com.jdroidev.rideagain.utils.Utils;

public class Main extends FragmentActivity {

  private InterstitialAd interstitial;
  private Dialog riderSettingsDialog;
  protected Rider rider;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    rider = SharedPreferencesStorage.get("rider", Rider.class,
        getApplicationContext());
    if (rider == null) {
      rider = new Rider("", "es", 1);
    }

    // Look up the AdView as a resource and load a request.
    AdView adView = (AdView) this.findViewById(R.id.adView);
    if (Utils.isFreeVersion) {
      AdRequest adRequest = new AdRequest.Builder().addTestDevice(
          "33B892C281D109B2").build();
      adView.loadAd(adRequest);
    } else {
      adView.setVisibility(View.GONE);
    }

    Button playButton = (Button) findViewById(R.id.playButton);
    playButton.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        if (SharedPreferencesStorage.get("rider", Rider.class,
            getApplicationContext()) == null) {
          openRiderSettingsDialog();
          return;
        }

        Intent intent = new Intent(getApplicationContext(),
            LevelSelection.class);
        startActivity(intent);
        finish();
      }
    });

    Button settingsButton = (Button) findViewById(R.id.settingsButton);
    settingsButton.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        Intent intent = new Intent(getApplicationContext(), Settings.class);
        startActivity(intent);
        finish();
      }
    });

    Button exitButton = (Button) findViewById(R.id.exitButton);
    exitButton.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        finish();
      }
    });

    if (Utils.isFreeVersion) {
      // Create the interstitial.
      interstitial = new InterstitialAd(this);
      interstitial.setAdUnitId("ca-app-pub-3225143533260426/8901765197");

      // Create ad request.
      AdRequest adRequest = new AdRequest.Builder().addTestDevice(
          "33B892C281D109B2").build();

      // Set the AdListener.
      interstitial.setAdListener(new AdListener() {
        @Override
        public void onAdLoaded() {
          displayInterstitial();
        }

        @Override
        public void onAdFailedToLoad(int errorCode) {
        }
      });

      // Begin loading your interstitial.
      interstitial.loadAd(adRequest);
    }
  }

  // Invoke displayInterstitial() when you are ready to display an
  // interstitial.
  public void displayInterstitial() {
    if (interstitial.isLoaded()) {
      interstitial.show();
    }
  }

  protected void openRiderSettingsDialog() {
    riderSettingsDialog = new Dialog(Main.this);
    riderSettingsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    riderSettingsDialog.setContentView(R.layout.dialog_rider_settings);
    riderSettingsDialog.setCanceledOnTouchOutside(false);
    riderSettingsDialog.setCancelable(false);

    Button saveRiderSettingsButton = (Button) riderSettingsDialog
        .findViewById(R.id.saveRiderSettingsButton);
    saveRiderSettingsButton.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        EditText riderName = (EditText) riderSettingsDialog
            .findViewById(R.id.riderNameField);
        rider.name = riderName.getText().toString();

        SharedPreferencesStorage.save(rider, "rider", getApplicationContext());

        riderSettingsDialog.dismiss();

        Intent intent = new Intent(getApplicationContext(),
            LevelSelection.class);
        startActivity(intent);
        finish();

      }
    });

    Button cancelRiderSettingsButton = (Button) riderSettingsDialog
        .findViewById(R.id.cancelRiderSettingsButton);
    cancelRiderSettingsButton.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {

        riderSettingsDialog.dismiss();

      }
    });

    EditText riderNameField = (EditText) riderSettingsDialog
        .findViewById(R.id.riderNameField);
    riderNameField.setText(rider.name);

    ImageView riderFlagImage = (ImageView) riderSettingsDialog
        .findViewById(R.id.riderFlag);
    String drawableName = "flag_" + rider.flag;
    riderFlagImage.setImageResource(getResId(drawableName));
    riderFlagImage.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        final CountryPicker picker = CountryPicker.newInstance(getResources()
            .getString(R.string.flag));
        picker.setListener(new CountryPickerListener() {

          @Override
          public void onSelectCountry(String name, String code) {
            ImageView riderFlagImage = (ImageView) riderSettingsDialog
                .findViewById(R.id.riderFlag);
            // Load drawable dynamically from country code
            String drawableName = "flag_" + code.toLowerCase(Locale.ENGLISH);
            riderFlagImage.setImageResource(getResId(drawableName));

            rider.flag = code.toLowerCase(Locale.ENGLISH);
            picker.dismiss();
          }
        });

        picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
      }

    });

    Wheel wheel = Wheel.load(rider.wheelId);

    ImageView riderWheelImage = (ImageView) riderSettingsDialog
        .findViewById(R.id.riderWheelModel);
    riderWheelImage.setImageResource(getResources().getIdentifier(wheel.design,
        "drawable", getPackageName()));
    riderWheelImage.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        final WheelModelPicker picker = WheelModelPicker
            .newInstance(getResources().getString(R.string.design));
        picker.setListener(new WheelPickerListener() {
          @Override
          public void onSelectWheel(Integer id) {
            ImageView riderWheelModel = (ImageView) riderSettingsDialog
                .findViewById(R.id.riderWheelModel);
            Wheel wheel = Wheel.load(id);            
            riderWheelModel.setImageResource(getResources().getIdentifier(
                wheel.design, "drawable", getPackageName()));
            rider.wheelId = id;
            picker.dismiss();

          }
        });

        picker.show(getSupportFragmentManager(), "WHEEL_PICKER");
      }

    });

    riderSettingsDialog.show();

  }

  /**
   * The drawable image name has the format "flag_$countryCode". We need to load
   * the drawable dynamically from country code. Code from
   * http://stackoverflow.com/
   * questions/3042961/how-can-i-get-the-resource-id-of
   * -an-image-if-i-know-its-name
   * 
   * @param drawableName
   * @return
   */
  private int getResId(String drawableName) {

    try {
      Class<com.jdroidev.rideagain.R.drawable> res = R.drawable.class;
      Field field = res.getField(drawableName);
      int drawableId = field.getInt(null);
      return drawableId;
    } catch (Exception e) {
      Log.e("COUNTRYPICKER", "Failure to get drawable id.", e);
    }
    return -1;
  }

}