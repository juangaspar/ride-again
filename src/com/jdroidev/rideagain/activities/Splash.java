package com.jdroidev.rideagain.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.jdroidev.rideagain.R;
import com.jdroidev.rideagain.utils.Utils;

public class Splash extends Activity {
	// Set the display time, in milliseconds (or extract it out as a
	// configurable
	// parameter)
	private final int SPLASH_DISPLAY_LENGTH = 1000;
	private Handler handler;
	private Runnable runnable;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		// Look up the AdView as a resource and load a request.
		AdView adView = (AdView) this.findViewById(R.id.adView);
		if (Utils.isFreeVersion) {
			AdRequest adRequest = new AdRequest.Builder().addTestDevice(
					"33B892C281D109B2").build();
			adView.loadAd(adRequest);
		} else {
			adView.setVisibility(View.GONE);
		}

		handler = new Handler();
		runnable = new Runnable() {
			@Override
			public void run() {
				Intent intent = new Intent(getApplicationContext(), Main.class);
				startActivity(intent);
				finish();
				return;

			}
		};

		handler.postDelayed(runnable, SPLASH_DISPLAY_LENGTH);
	}

	@Override
	protected void onPause() {

		// TODO Auto-generated method stub
		super.onPause();
	}
}