package com.jdroidev.rideagain.activities;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.andengine.engine.Engine;
import org.andengine.engine.FixedStepEngine;
import org.andengine.engine.camera.SmoothCamera;
import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.camera.hud.controls.AnalogOnScreenControl;
import org.andengine.engine.camera.hud.controls.AnalogOnScreenControl.IAnalogOnScreenControlListener;
import org.andengine.engine.camera.hud.controls.BaseOnScreenControl;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.particle.SpriteParticleSystem;
import org.andengine.entity.particle.emitter.PointParticleEmitter;
import org.andengine.entity.particle.initializer.ExpireParticleInitializer;
import org.andengine.entity.particle.initializer.VelocityParticleInitializer;
import org.andengine.entity.primitive.Line;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.shape.IShape;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.ui.activity.LayoutGameActivity;
import org.andengine.util.adt.align.HorizontalAlign;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.hardware.SensorManager;
import android.os.CountDownTimer;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.jdroidev.rideagain.R;
import com.jdroidev.rideagain.game.Bike;
import com.jdroidev.rideagain.game.BikeStep;
import com.jdroidev.rideagain.game.BikeStepsPackage;
import com.jdroidev.rideagain.game.GhostBike;
import com.jdroidev.rideagain.game.Level;
import com.jdroidev.rideagain.game.Rider;
import com.jdroidev.rideagain.game.SharedPreferencesStorage;
import com.jdroidev.rideagain.game.Track;
import com.jdroidev.rideagain.game.trackelements.Checkpoint;
import com.jdroidev.rideagain.game.trackelements.TrackBooster;
import com.jdroidev.rideagain.game.trackelements.TrackBox;
import com.jdroidev.rideagain.game.trackelements.TrackCircle;
import com.jdroidev.rideagain.game.trackelements.TrackDynamicPlatform;
import com.jdroidev.rideagain.game.trackelements.TrackEventPlatform;
import com.jdroidev.rideagain.game.trackelements.TrackGravityModifier;
import com.jdroidev.rideagain.game.trackelements.TrackLine;
import com.jdroidev.rideagain.game.trackelements.TrackLinesGroup;
import com.jdroidev.rideagain.game.trackelements.TrackPipe;
import com.jdroidev.rideagain.game.trackelements.TrackRecord;
import com.jdroidev.rideagain.game.trackelements.TrackTPlatform;
import com.jdroidev.rideagain.game.trackelements.TrackZoomPoint;
import com.jdroidev.rideagain.tracks.LevelsDefinition;
import com.jdroidev.rideagain.utils.Installation;
import com.jdroidev.rideagain.utils.Utils;

public class Game extends LayoutGameActivity implements IOnSceneTouchListener {

  // ===========================================================
  // Constants
  // ===========================================================
  public static final short CATEGORYBIT_NO = 0;
  public static final short CATEGORYBIT_WALL = 1;
  public static final short CATEGORYBIT_BOX = 2;
  public static final short CATEGORYBIT_CIRCLE = 4;
  public static final short MASKBITS_WALL = CATEGORYBIT_WALL + CATEGORYBIT_BOX
      + CATEGORYBIT_CIRCLE;
  public static final short MASKBITS_BOX = CATEGORYBIT_WALL + CATEGORYBIT_BOX; // Missing:
  // CATEGORYBIT_CIRCLE
  public static final short MASKBITS_CIRCLE = CATEGORYBIT_WALL
      + CATEGORYBIT_CIRCLE; // Missing: CATEGORYBIT_BOX
  public static final short MASKBITS_NOTHING = 0; // Missing: all
  public static final short MASKBITS_ONLY_WALL = CATEGORYBIT_WALL; // Missing:

  // wall
  public static final FixtureDef WALL_FIXTURE_DEF = PhysicsFactory
      .createFixtureDef(0, 0.2f, 1.1f, false, CATEGORYBIT_WALL, MASKBITS_WALL,
          (short) 0);
  public static final FixtureDef BOX_FIXTURE_DEF = PhysicsFactory
      .createFixtureDef(1, 0.5f, 0.5f, false, CATEGORYBIT_BOX, MASKBITS_BOX,
          (short) 0);
  public static final FixtureDef CIRCLE_FIXTURE_DEF = PhysicsFactory
      .createFixtureDef(1, 0.5f, 0.5f, false, CATEGORYBIT_CIRCLE,
          MASKBITS_CIRCLE, (short) 0);
  public static final FixtureDef NO_FIXTURE_DEF = PhysicsFactory
      .createFixtureDef(1, 0.5f, 0.5f, false, CATEGORYBIT_NO, MASKBITS_NOTHING,
          (short) 0);
  public static final FixtureDef ONLY_WALL_FIXTURE_DEF = PhysicsFactory
      .createFixtureDef(1, 0.2f, 1f, false, CATEGORYBIT_CIRCLE,
          MASKBITS_ONLY_WALL, (short) -1);

  private static final int STATUS_LOADING = 0;
  private static final int STATUS_RUNNING = 1;
  private static final int STATUS_END = 2;
  private static final int STATUS_PAUSED = 3;

  // ====================================================
  // CONSTANTS
  // ====================================================
  public static int cameraWidth = 800;
  public static int cameraHeight = 480;

  // ====================================================
  // VARIABLES
  // ====================================================
  public Scene mScene;
  public FixedStepPhysicsWorld mPhysicsWorld;

  private TiledTextureRegion mButtonAccelerateTextureRegion;
  private TiledTextureRegion mButtonBrakeTextureRegion;
  private BitmapTextureAtlas mOnScreenControlTexture;
  private TextureRegion mOnScreenControlBaseTextureRegion;
  private TextureRegion mOnScreenControlKnobTextureRegion;
  private SmoothCamera mCamera;
  protected float analogControllerXValue;
  private HUD mHud;
  private Bike bike;
  private TextureRegion circleBlack48TextureRegion;
  private TextureRegion checkpointBlueTextureRegion;
  private TextureRegion checkpointOrangeTextureRegion;
  private Font fontTimer;
  private Font fontGhostTimer;
  private Text timerText;

  // game vars
  private long startTime;
  private Sprite endCheckpointSprite;
  private Track track;
  private int status;
  private TrackRecord currentTrackRecord = new TrackRecord();
  private int stepCount;
  private Text ghostTimerText;
  private AnalogOnScreenControl analogOnScreenControl;
  private Font countdownTimerFont;
  private Text countdownText;
  private ButtonSprite buttonBrake;
  private ButtonSprite buttonAcc;
  private Rectangle timerFrame;
  private TextureRegion countdownTextureRegion;
  private Sprite countdownSprite;
  private Dialog scoreDialog;
  private long pauseTime;
  protected int nextTrack;
  private int activatedCheckpoints;
  private TiledTextureRegion buttonPauseTextureRegion;
  private ButtonSprite buttonPause;
  private Dialog pauseDialog;
  private Boolean ghostLocalModeEnabled;
  private Boolean ghostGlobalModeEnabled;
  private Rider rider;
  private Level level;
  public boolean isAccPressed = false;
  public boolean isBrakePressed = false;
  private TextureRegion gravityTextureRegion;
  private Integer currentTrackLightnings;
  private Integer currentLevelLightnings;
  private Integer totalLightnings;
  private TrackRecord bestPersonalTrackRecord;
  private GhostBike ghostPersonalBike;
  private TrackRecord bestGlobalTrackRecord;
  private GhostBike ghostGlobalBike;
  private TextureRegion boosterTextureRegion;
  private TextureRegion sandTextureRegion;
  private PointParticleEmitter wheelParticleEmitter;
  private SpriteParticleSystem wheelParticleSystem;
  private boolean rearWheelContactingGround = false;
  private VelocityParticleInitializer<Sprite> wheelVelocityParticleInitializer;
  private Vector2 rearWheelContactingDifference;

  @Override
  public Engine onCreateEngine(final EngineOptions pEngineOptions) {

    this.status = Game.STATUS_LOADING;
    this.ghostLocalModeEnabled = SharedPreferencesStorage.get(
        "ghostLocalModeEnabled", Boolean.class, getApplicationContext());

    this.ghostLocalModeEnabled = (this.ghostLocalModeEnabled == null) ? false
        : this.ghostLocalModeEnabled;

    this.ghostGlobalModeEnabled = SharedPreferencesStorage.get(
        "ghostGlobalModeEnabled", Boolean.class, getApplicationContext());

    this.ghostGlobalModeEnabled = (this.ghostGlobalModeEnabled == null) ? false
        : this.ghostGlobalModeEnabled;

    rider = SharedPreferencesStorage.get("rider", Rider.class,
        getApplicationContext());

    return new FixedStepEngine(pEngineOptions, 100);
  }

  // ====================================================
  // CREATE ENGINE OPTIONS
  // ====================================================
  @Override
  public EngineOptions onCreateEngineOptions() {
    this.mCamera = new SmoothCamera(0, 0, cameraWidth, cameraHeight, 0f, 0f,
        0.5f);
    EngineOptions engineOptions = new EngineOptions(true,
        ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(1.66f),
        this.mCamera);
    engineOptions.getRenderOptions().setDithering(true);
    engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);

    // create hud
    mHud = new HUD();
    mCamera.setHUD(mHud);

    engineOptions.getTouchOptions().setNeedsMultiTouch(true);
    return engineOptions;
  }

  // ====================================================
  // CREATE RESOURCES
  // ====================================================
  @Override
  public void onCreateResources(
      OnCreateResourcesCallback pOnCreateResourcesCallback) {

    // Load our font
    fontTimer = FontFactory.create(mEngine.getFontManager(),
        mEngine.getTextureManager(), 256, 256,
        Typeface.create(Typeface.DEFAULT, Typeface.NORMAL), 30f, true,
        Color.WHITE);
    fontTimer.load();

    countdownTimerFont = FontFactory.create(mEngine.getFontManager(),
        mEngine.getTextureManager(), 256, 256,
        Typeface.create(Typeface.DEFAULT, Typeface.NORMAL), 40f, true,
        Color.BLACK);
    countdownTimerFont.load();

    // Load our font
    fontGhostTimer = FontFactory.create(mEngine.getFontManager(),
        mEngine.getTextureManager(), 256, 256,
        Typeface.create(Typeface.DEFAULT, Typeface.NORMAL), 26f, true,
        Color.WHITE);
    fontGhostTimer.load();

    BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

    this.mOnScreenControlTexture = new BitmapTextureAtlas(
        this.getTextureManager(), 256, 128, TextureOptions.BILINEAR);
    this.mOnScreenControlBaseTextureRegion = BitmapTextureAtlasTextureRegionFactory
        .createFromAsset(this.mOnScreenControlTexture, this,
            "control_base.png", 0, 0);
    this.mOnScreenControlKnobTextureRegion = BitmapTextureAtlasTextureRegionFactory
        .createFromAsset(this.mOnScreenControlTexture, this,
            "control_knob.png", 128, 0);
    this.mOnScreenControlTexture.load();

    // load circle textures
    BitmapTextureAtlas circleBlack48Texture = new BitmapTextureAtlas(
        this.getTextureManager(), 48, 48, TextureOptions.BILINEAR);
    circleBlack48TextureRegion = BitmapTextureAtlasTextureRegionFactory
        .createFromAsset(circleBlack48Texture, this, "circles/black_48.png", 0,
            0);
    circleBlack48Texture.load();

    BitmapTextureAtlas countdownTexture = new BitmapTextureAtlas(
        this.getTextureManager(), 100, 100, TextureOptions.BILINEAR);
    countdownTextureRegion = BitmapTextureAtlasTextureRegionFactory
        .createFromAsset(countdownTexture, this, "countdown.png", 0, 0);
    countdownTexture.load();

    // load gravity textures
    BitmapTextureAtlas gravityTexture = new BitmapTextureAtlas(
        this.getTextureManager(), 48, 48, TextureOptions.BILINEAR);
    gravityTextureRegion = BitmapTextureAtlasTextureRegionFactory
        .createFromAsset(gravityTexture, this, "gravity_48.png", 0, 0);
    gravityTexture.load();

    // load gravity textures
    BitmapTextureAtlas boosterTexture = new BitmapTextureAtlas(
        this.getTextureManager(), 48, 48, TextureOptions.BILINEAR);
    boosterTextureRegion = BitmapTextureAtlasTextureRegionFactory
        .createFromAsset(boosterTexture, this, "battery_48.png", 0, 0);
    boosterTexture.load();

    // load checkpoint textures
    BitmapTextureAtlas checkpointBlueTexture = new BitmapTextureAtlas(
        this.getTextureManager(), 15, 90, TextureOptions.BILINEAR);
    BitmapTextureAtlas checkpointOrangeTexture = new BitmapTextureAtlas(
        this.getTextureManager(), 15, 90, TextureOptions.BILINEAR);
    checkpointBlueTextureRegion = BitmapTextureAtlasTextureRegionFactory
        .createFromAsset(checkpointBlueTexture, this, "checkpoints/blue.png",
            0, 0);
    checkpointOrangeTextureRegion = BitmapTextureAtlasTextureRegionFactory
        .createFromAsset(checkpointOrangeTexture, this,
            "checkpoints/orange.png", 0, 0);
    checkpointBlueTexture.load();
    checkpointOrangeTexture.load();

    BitmapTextureAtlas sandTexture = new BitmapTextureAtlas(
        mEngine.getTextureManager(), 4, 4, TextureOptions.BILINEAR);

    sandTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
        sandTexture, this, "sand.png", 0, 0);
    sandTexture.load();

    /*
     * Create the bitmap texture atlas.
     * 
     * The bitmap texture atlas is created to fit a texture region of 300x50
     * pixels
     */
    BuildableBitmapTextureAtlas bitmapTextureAtlas = new BuildableBitmapTextureAtlas(
        mEngine.getTextureManager(), 260, 130);

    /* Create the buttons texture region with 2 columns, 1 row */
    mButtonAccelerateTextureRegion = BitmapTextureAtlasTextureRegionFactory
        .createTiledFromAsset(bitmapTextureAtlas, getAssets(),
            "buttons_speed.png", 2, 1);

    BuildableBitmapTextureAtlas bitmapTextureAtlas2 = new BuildableBitmapTextureAtlas(
        mEngine.getTextureManager(), 220, 110);
    /* Create the buttons texture region with 2 columns, 1 row */
    mButtonBrakeTextureRegion = BitmapTextureAtlasTextureRegionFactory
        .createTiledFromAsset(bitmapTextureAtlas2, getAssets(),
            "buttons_brake.png", 2, 1);

    BuildableBitmapTextureAtlas bitmapTextureAtlas3 = new BuildableBitmapTextureAtlas(
        mEngine.getTextureManager(), 100, 50);
    /* Create the buttons texture region with 2 columns, 1 row */
    buttonPauseTextureRegion = BitmapTextureAtlasTextureRegionFactory
        .createTiledFromAsset(bitmapTextureAtlas3, getAssets(),
            "buttons_pause.png", 2, 1);

    /* Build the bitmap texture atlas */
    try {
      bitmapTextureAtlas
          .build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
              0, 0, 0));
      bitmapTextureAtlas2
          .build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
              0, 0, 0));
      bitmapTextureAtlas3
          .build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
              0, 0, 0));
    } catch (TextureAtlasBuilderException e) {
      e.printStackTrace();
    }

    /* Load the bitmap texture atlas */
    bitmapTextureAtlas.load();
    bitmapTextureAtlas2.load();
    bitmapTextureAtlas3.load();

    pOnCreateResourcesCallback.onCreateResourcesFinished();
  }

  // ====================================================
  // CREATE SCENE
  // ====================================================
  @Override
  public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) {
    mScene = new Scene();

    mEngine.registerUpdateHandler(new myUpdateHandler());
    mEngine.registerUpdateHandler(new myCollidesHandler());

    mScene.setBackground(new Background(0.9f, 0.9f, 0.9f));

    analogOnScreenControl = new AnalogOnScreenControl(
        mOnScreenControlBaseTextureRegion.getWidth(),
        mOnScreenControlBaseTextureRegion.getHeight() / 1.6f, this.mCamera,
        this.mOnScreenControlBaseTextureRegion,
        this.mOnScreenControlKnobTextureRegion, 0.1f,
        this.getVertexBufferObjectManager(),
        new IAnalogOnScreenControlListener() {
          @Override
          public void onControlChange(
              final BaseOnScreenControl pBaseOnScreenControl,
              final float pValueX, final float pValueY) {
            analogControllerXValue = pValueX;
          }

          @Override
          public void onControlClick(
              final AnalogOnScreenControl pAnalogOnScreenControl) {
          }

        });

    analogOnScreenControl.refreshControlKnobPosition();
    analogOnScreenControl.setVisible(false);
    analogOnScreenControl.setAlpha(0.6f);
    mScene.setChildScene(analogOnScreenControl);

    pOnCreateSceneCallback.onCreateSceneFinished(mScene);
  }

  // ====================================================
  // POPULATE SCENE
  // ====================================================

  @Override
  public void onPopulateScene(Scene pScene,
      OnPopulateSceneCallback pOnPopulateSceneCallback) {

    mPhysicsWorld = new FixedStepPhysicsWorld(100, new Vector2(0f,
        -SensorManager.GRAVITY_EARTH), false, 100, 100);

    mScene.registerUpdateHandler(mPhysicsWorld);
    mPhysicsWorld.setContactListener(new ContactListener() {

      private Vector2 contactingPoint;

      @Override
      public void preSolve(Contact contact, Manifold oldManifold) {

      }

      @Override
      public void postSolve(Contact contact, ContactImpulse impulse) {
        // TODO Auto-generated method stub

      }

      @Override
      public void endContact(Contact contact) {

        // check rear wheel collision to show particles
        if (contact.getFixtureA().getBody().equals(bike.getRearWheelBody())
            || contact.getFixtureB().getBody().equals(bike.getRearWheelBody())) {
          runOnUpdateThread(new Runnable() {

            @Override
            public void run() {
              rearWheelContactingGround = false;
            }
          });
        }
      }

      @Override
      public void beginContact(Contact contact) {
        // check head collision to respawn
        if (contact.getFixtureA().getBody().equals(bike.getRiderHeadBody())
            || contact.getFixtureB().getBody().equals(bike.getRiderHeadBody())) {
          runOnUpdateThread(new Runnable() {

            @Override
            public void run() {
              respawnBike();
            }
          });
        }

        // check rear wheel collision to show particles
        if (contact.getFixtureA().getBody().equals(bike.getRearWheelBody())
            || contact.getFixtureB().getBody().equals(bike.getRearWheelBody())) {
          contactingPoint = contact.getWorldManifold().getPoints()[0];
          runOnUpdateThread(new Runnable() {

            @Override
            public void run() {
              rearWheelContactingDifference = new Vector2(bike
                  .getRearWheelBody().getWorldCenter().x - contactingPoint.x,
                  bike.getRearWheelBody().getWorldCenter().y
                      - contactingPoint.y);
              rearWheelContactingGround = true;
            }
          });
        }

      }
    });

    // create balance buttons
    createBalanceButtons();

    // initialize bike
    bike = new Bike(getApplicationContext(), rider);

    // initialize level
    initializeLevel();

    // set up track record
    setUpTrackRecord();

    createTimer();

    bike.initialize(mScene, mPhysicsWorld, track.bikeInitialSpawnPosition,
        getVertexBufferObjectManager(), getTextureManager(), true);

    if (ghostLocalModeEnabled && bestPersonalTrackRecord != null) {
      // initialize bike
      ghostPersonalBike = new GhostBike(getApplicationContext(),
          bestPersonalTrackRecord.r, false);
      ghostPersonalBike
          .initialize(mScene, track.bikeInitialSpawnPosition,
              getVertexBufferObjectManager(), getTextureManager(),
              getFontManager());
    }

    if (ghostGlobalModeEnabled && bestGlobalTrackRecord != null) {
      // initialize bike
      ghostGlobalBike = new GhostBike(getApplicationContext(),
          bestGlobalTrackRecord.r, true);
      ghostGlobalBike
          .initialize(mScene, track.bikeInitialSpawnPosition,
              getVertexBufferObjectManager(), getTextureManager(),
              getFontManager());
    }

    createParticleSystem();

    mHud.setOnSceneTouchListener(this);
    pOnPopulateSceneCallback.onPopulateSceneFinished();
  }

  private void createParticleSystem() {
    // Create the particle emitter
    wheelParticleEmitter = new PointParticleEmitter(0, 0);
    // Create the particle system
    wheelParticleSystem = new SpriteParticleSystem(wheelParticleEmitter, 45,
        50, 50, sandTextureRegion, mEngine.getVertexBufferObjectManager());

    wheelParticleSystem
        .addParticleInitializer(new ExpireParticleInitializer<Sprite>(0.1f,
            0.2f));
    wheelVelocityParticleInitializer = new VelocityParticleInitializer<Sprite>(
        -5f, -50f, 50f, 100f);
    wheelParticleSystem
        .addParticleInitializer(wheelVelocityParticleInitializer);

    wheelParticleSystem.setParticlesSpawnEnabled(false);
    // Attach our particle system to the scene
    mScene.attachChild(wheelParticleSystem);
  }

  private void setUpTrackRecord() {
    currentTrackRecord = new TrackRecord();
    currentTrackRecord.i = this.track.id;
    currentTrackRecord.l = this.level.id;
    currentTrackRecord.m = this.level.mode;
    currentTrackRecord.r = this.rider;
    currentTrackRecord.u = Installation.id(getApplicationContext());
    currentTrackRecord.b = new BikeStepsPackage();

    bestPersonalTrackRecord = SharedPreferencesStorage.get("trackRecord_"
        + this.track.id + "_" + this.level.mode, TrackRecord.class,
        getApplicationContext());
    if (bestPersonalTrackRecord != null) {
      bestPersonalTrackRecord.b = SharedPreferencesStorage.get("trackRecord_"
          + this.track.id + "_" + this.level.mode + "_steps",
          BikeStepsPackage.class, getApplicationContext());
    }

    bestGlobalTrackRecord = SharedPreferencesStorage.get("globalTrackRecord_"
        + this.track.id + "_" + this.level.mode, TrackRecord.class,
        getApplicationContext());
    if (bestGlobalTrackRecord != null) {
      bestGlobalTrackRecord.b = SharedPreferencesStorage.get(
          "globalTrackRecord_" + this.track.id + "_" + this.level.mode
              + "_steps", BikeStepsPackage.class, getApplicationContext());
    }
  }

  private void createTimer() {

    // Create TextOptions for our text
    TextOptions textOptions = new TextOptions();
    textOptions.setHorizontalAlign(HorizontalAlign.CENTER);

    timerFrame = new Rectangle(700, 440, 160, 40,
        getVertexBufferObjectManager());
    timerFrame.setColor(Color.BLACK);
    timerFrame.setAlpha(0.3f);
    timerFrame.setVisible(false);
    timerFrame.setIgnoreUpdate(true);
    mHud.attachChild(timerFrame);

    // Create our text object
    timerText = new Text(700, 440, fontTimer, "00:00.000",
        "00:00.000".length(), textOptions,
        mEngine.getVertexBufferObjectManager());

    // Apply our text to the scene
    timerText.setVisible(false);
    mHud.attachChild(timerText);

    if (ghostLocalModeEnabled && bestPersonalTrackRecord != null) {
      ghostTimerText = new Text(700, 400, fontGhostTimer, "- 00:00.000",
          "- 00:00:000".length(), textOptions,
          mEngine.getVertexBufferObjectManager());
      ghostTimerText.setVisible(false);
      // Apply our text to the scene
      mHud.attachChild(ghostTimerText);
    }

    countdownSprite = new Sprite(cameraWidth / 2, 380, countdownTextureRegion,
        getVertexBufferObjectManager());

    countdownSprite.setCullingEnabled(true);
    countdownSprite.setIgnoreUpdate(true);
    mHud.attachChild(countdownSprite);

    // Create count down text
    countdownText = new Text(cameraWidth / 2, 380, countdownTimerFont, "3 ",
        "000".length(), textOptions, mEngine.getVertexBufferObjectManager());

    mHud.attachChild(countdownText);

  }

  private void initializeLevel() {
    int trackId = getIntent().getIntExtra("trackId", 1);
    int levelId = getIntent().getIntExtra("levelId", 1);

    LevelsDefinition levelsDefinition = new LevelsDefinition();

    for (Level level : levelsDefinition.levels) {
      if (level.id == levelId) {
        this.level = level;
        break;
      }
    }

    for (int i = 0; i < this.level.tracks.size(); i++) {
      if (this.level.tracks.get(i).id == trackId) {
        track = this.level.tracks.get(i);
        drawTrack(track);
        break;
      }
    }

    // initialize current track lightnings
    currentTrackLightnings = SharedPreferencesStorage.get("trackLightnings_"
        + trackId + "_" + levelId, Integer.class, getApplicationContext());
    currentTrackLightnings = (currentTrackLightnings == null) ? 0
        : currentTrackLightnings;
    currentLevelLightnings = SharedPreferencesStorage.get("levelLightnings_"
        + levelId, Integer.class, getApplicationContext());
    currentLevelLightnings = (currentLevelLightnings == null) ? 0
        : currentLevelLightnings;

    totalLightnings = SharedPreferencesStorage.get("totalLightnings",
        Integer.class, getApplicationContext());
    totalLightnings = (totalLightnings == null) ? 0 : totalLightnings;
  }

  private void drawTrack(Track track) {

    mCamera.setZoomFactor(track.initialZoomFactor);

    // draw lines
    for (TrackLine trackLine : track.trackLines) {
      Line l = new Line(trackLine.initialPoint.x, trackLine.initialPoint.y,
          trackLine.endPoint.x, trackLine.endPoint.y,
          getVertexBufferObjectManager());
      l.setLineWidth(trackLine.width);
      l.setColor(Color.parseColor(trackLine.color));
      l.setIgnoreUpdate(true);
      mScene.attachChild(l);
      PhysicsFactory.createLineBody(mPhysicsWorld, l, WALL_FIXTURE_DEF);
    }

    // draw track lines groups
    for (TrackLinesGroup trackLinesGroup : track.trackLinesGroups) {
      for (int i = 0; i < trackLinesGroup.initialPoints.size(); i++) {
        Line l = new Line(trackLinesGroup.initialPoints.get(i).x,
            trackLinesGroup.initialPoints.get(i).y,
            trackLinesGroup.endPoints.get(i).x,
            trackLinesGroup.endPoints.get(i).y, getVertexBufferObjectManager());
        l.setLineWidth(trackLinesGroup.width);
        l.setColor(Color.parseColor(trackLinesGroup.color));
        l.setIgnoreUpdate(true);
        mScene.attachChild(l);
        PhysicsFactory.createLineBody(mPhysicsWorld, l, WALL_FIXTURE_DEF);
      }
    }

    // draw boxes
    for (TrackBox trackBox : track.trackBoxes) {
      Rectangle trackBoxRectangle = new Rectangle(trackBox.position.x,
          trackBox.position.y, trackBox.width, trackBox.height,
          getVertexBufferObjectManager());
      trackBoxRectangle.setColor(Color.parseColor(trackBox.color));
      trackBoxRectangle.setIgnoreUpdate(true);
      trackBoxRectangle.setCullingEnabled(true);
      mScene.attachChild(trackBoxRectangle);
      Body trackBoxBody = PhysicsFactory.createBoxBody(mPhysicsWorld,
          trackBoxRectangle, BodyType.StaticBody, WALL_FIXTURE_DEF);
      mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(
          trackBoxRectangle, trackBoxBody, true, true));
    }

    // draw circles
    for (TrackCircle trackCircle : track.trackCircles) {
      TextureRegion texture = null;
      texture = circleBlack48TextureRegion;
      Sprite circleSprite = new Sprite(trackCircle.position.x,
          trackCircle.position.y, texture, getVertexBufferObjectManager());

      circleSprite.setCullingEnabled(true);
      circleSprite.setIgnoreUpdate(true);
      mScene.attachChild(circleSprite);
      PhysicsFactory.createCircleBody(mPhysicsWorld, circleSprite,
          BodyType.StaticBody, WALL_FIXTURE_DEF);
    }

    // draw tplatforms
    for (TrackTPlatform trackTplatform : track.trackTPlatforms) {
      Rectangle base = new Rectangle(trackTplatform.basePosition.x
          + trackTplatform.tOffset, trackTplatform.basePosition.y
          + trackTplatform.height / 2, trackTplatform.baseWidth,
          trackTplatform.height, getVertexBufferObjectManager());

      base.setColor(Color.parseColor(trackTplatform.baseColor));
      base.setIgnoreUpdate(true);
      base.setCullingEnabled(true);
      mScene.attachChild(base);
      Body baseBody = PhysicsFactory.createBoxBody(mPhysicsWorld, base,
          BodyType.StaticBody, WALL_FIXTURE_DEF);

      Rectangle topPlatform = new Rectangle(trackTplatform.basePosition.x
          + trackTplatform.tOffset, trackTplatform.basePosition.y
          + trackTplatform.height, trackTplatform.tLongitude,
          trackTplatform.baseWidth, getVertexBufferObjectManager());
      topPlatform.setColor(Color.parseColor(trackTplatform.tColor));
      topPlatform.setIgnoreUpdate(true);
      topPlatform.setCullingEnabled(true);
      mScene.attachChild(topPlatform);
      Body topPlatformBody = PhysicsFactory.createBoxBody(mPhysicsWorld,
          topPlatform, BodyType.DynamicBody, PhysicsFactory.createFixtureDef(1,
              0.2f, 1f, false, CATEGORYBIT_WALL, MASKBITS_WALL, (short) 0));
      mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(topPlatform,
          topPlatformBody, true, true));

      RevoluteJointDef topPlatformJointDef = new RevoluteJointDef();
      topPlatformJointDef.upperAngle = trackTplatform.tUpperAngle
          / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;
      topPlatformJointDef.lowerAngle = trackTplatform.tLowerAngle
          / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;
      topPlatformJointDef.enableLimit = true;
      topPlatformJointDef.initialize(baseBody, topPlatformBody,
          topPlatformBody.getWorldCenter());

      trackTplatform.joint = (RevoluteJoint) mPhysicsWorld
          .createJoint(topPlatformJointDef);
      trackTplatform.joint.getBodyB().applyTorque(trackTplatform.tRotation);
    }

    // draw dynamic platforms
    for (TrackDynamicPlatform trackDynamicPlatform : track.trackDynamicPlatforms) {

      Rectangle box = new Rectangle(trackDynamicPlatform.position.x,
          trackDynamicPlatform.position.y, trackDynamicPlatform.longitude,
          trackDynamicPlatform.width, getVertexBufferObjectManager());
      box.setColor(Color.parseColor(trackDynamicPlatform.color));
      box.setCullingEnabled(true);
      box.setIgnoreUpdate(true);
      mScene.attachChild(box);
      Body boxBody = PhysicsFactory.createBoxBody(mPhysicsWorld, box,
          BodyType.KinematicBody, PhysicsFactory.createFixtureDef(1, 0.2f, 1f,
              false, CATEGORYBIT_WALL, MASKBITS_WALL, (short) 0));

      trackDynamicPlatform.box = boxBody;
      mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(box, boxBody,
          true, true));
    }

    // draw checkpoints
    for (Checkpoint checkpoint : track.checkpoints) {
      // draw end checkpoint
      TextureRegion checkpointTexture = null;
      switch (checkpoint.color) {
      case Checkpoint.COLOR_BLUE:
        checkpointTexture = checkpointBlueTextureRegion;
        break;
      case Checkpoint.COLOR_ORANGE:
        checkpointTexture = checkpointOrangeTextureRegion;
        break;

      default:
        break;
      }

      Sprite checkpointSprite = new Sprite(checkpoint.position.x,
          checkpoint.position.y, checkpointTexture,
          getVertexBufferObjectManager());
      checkpointSprite.setZIndex(20);

      checkpointSprite.setCullingEnabled(true);
      checkpointSprite.setIgnoreUpdate(true);
      checkpoint.sprite = checkpointSprite;
      mScene.attachChild(checkpointSprite);
    }

    // draw zoom points
    for (TrackZoomPoint zoomPoint : track.trackZoomPoints) {
      Sprite zoomPointSprite = new Sprite(zoomPoint.position.x,
          zoomPoint.position.y, checkpointBlueTextureRegion,
          getVertexBufferObjectManager());
      zoomPointSprite.setVisible(false);

      zoomPointSprite.setCullingEnabled(true);
      zoomPointSprite.setIgnoreUpdate(true);
      zoomPoint.zoomEntity = zoomPointSprite;
      mScene.attachChild(zoomPointSprite);
    }

    // draw gravity modifiers
    for (TrackGravityModifier gravityModifier : track.trackGravityModifiers) {
      Sprite gravityModifierSprite = new Sprite(gravityModifier.position.x,
          gravityModifier.position.y, gravityTextureRegion,
          getVertexBufferObjectManager());
      gravityModifierSprite.setVisible(true);
      gravityModifierSprite.setCullingEnabled(true);
      gravityModifierSprite.setIgnoreUpdate(true);
      gravityModifier.gravityEntity = gravityModifierSprite;
      mScene.attachChild(gravityModifierSprite);
    }

    // draw boosters
    for (TrackBooster booster : track.trackBoosters) {
      Sprite boosterSprite = new Sprite(booster.position.x, booster.position.y,
          boosterTextureRegion, getVertexBufferObjectManager());
      boosterSprite.setVisible(true);
      boosterSprite.setCullingEnabled(true);
      boosterSprite.setIgnoreUpdate(true);
      booster.sprite = boosterSprite;
      mScene.attachChild(boosterSprite);
    }

    // draw event platforms
    for (TrackEventPlatform trackEventPlatform : track.trackEventPlatforms) {
      Line l = new Line(trackEventPlatform.initialPoint.x,
          trackEventPlatform.initialPoint.y, trackEventPlatform.endPoint.x,
          trackEventPlatform.endPoint.y, getVertexBufferObjectManager());
      l.setLineWidth(trackEventPlatform.width);
      l.setColor(Color.parseColor(trackEventPlatform.color));
      l.setIgnoreUpdate(true);
      mScene.attachChild(l);
      trackEventPlatform.line = l;
      trackEventPlatform.lineBody = PhysicsFactory.createLineBody(
          mPhysicsWorld, l, WALL_FIXTURE_DEF);

      Sprite eventPointSprite = new Sprite(trackEventPlatform.eventPoint.x,
          trackEventPlatform.eventPoint.y, checkpointBlueTextureRegion,
          getVertexBufferObjectManager());
      eventPointSprite.setVisible(false);

      eventPointSprite.setCullingEnabled(true);
      eventPointSprite.setIgnoreUpdate(true);
      trackEventPlatform.eventPointSprite = eventPointSprite;
      mScene.attachChild(eventPointSprite);

      if (trackEventPlatform.event == TrackEventPlatform.SET_VISIBLE) {
        trackEventPlatform.line.setVisible(false);
        trackEventPlatform.lineBody.setActive(false);
      }
    }

    // draw track pipes
    for (TrackPipe trackPipe : track.trackPipes) {
      for (int i = 0; i < trackPipe.initialPoints.size(); i++) {
        Line l = new Line(trackPipe.initialPoints.get(i).x,
            trackPipe.initialPoints.get(i).y, trackPipe.endPoints.get(i).x,
            trackPipe.endPoints.get(i).y, getVertexBufferObjectManager());
        l.setLineWidth(trackPipe.width);
        l.setColor(Color.parseColor(trackPipe.color));
        l.setIgnoreUpdate(true);
        mScene.attachChild(l);
        PhysicsFactory.createLineBody(mPhysicsWorld, l, WALL_FIXTURE_DEF);
      }
    }

    // draw end checkpoint
    TextureRegion endTexture = null;
    switch (track.endCheckpoint.color) {
    case Checkpoint.COLOR_BLUE:
      endTexture = checkpointBlueTextureRegion;
      break;
    case Checkpoint.COLOR_ORANGE:
      endTexture = checkpointOrangeTextureRegion;
      break;

    default:
      break;
    }

    endCheckpointSprite = new Sprite(track.endCheckpoint.position.x,
        track.endCheckpoint.position.y, endTexture,
        getVertexBufferObjectManager());
    endCheckpointSprite.setZIndex(20);

    endCheckpointSprite.setCullingEnabled(true);
    endCheckpointSprite.setIgnoreUpdate(true);
    mScene.attachChild(endCheckpointSprite);
  }

  private void createBalanceButtons() {
    buttonBrake = new ButtonSprite(580, 70, mButtonBrakeTextureRegion,
        mEngine.getVertexBufferObjectManager());

    buttonBrake.setVisible(false);
    buttonBrake.setAlpha(0.5f);
    mHud.attachChild(buttonBrake);

    buttonAcc = new ButtonSprite(700, 80, mButtonAccelerateTextureRegion,
        mEngine.getVertexBufferObjectManager());

    buttonAcc.setVisible(false);
    buttonAcc.setAlpha(0.5f);
    mHud.attachChild(buttonAcc);

    buttonPause = new ButtonSprite(60, 430, buttonPauseTextureRegion,
        getVertexBufferObjectManager());
    buttonPause.setOnClickListener(new ButtonSprite.OnClickListener() {

      @Override
      public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX,
          float pTouchAreaLocalY) {
        pauseGame();
      }
    });

    buttonPause.setVisible(false);
    buttonPause.setAlpha(0.5f);
    mHud.registerTouchArea(buttonPause);
    mHud.attachChild(buttonPause);

  }

  protected boolean isTouchInside(IShape shape, float localX, float localY) {
    float halfWidth = shape.getWidth() / 2;
    float halfHeight = shape.getHeight() / 2;
    boolean insideX = ((localX >= shape.getX() - halfWidth) && (localX <= shape
        .getX() + halfWidth));
    boolean insideY = ((localY >= shape.getY() - halfHeight) && (localY <= shape
        .getY() + halfHeight));

    if (insideX && insideY) {
      return true;
    } else {
      return false;
    }
  }

  @Override
  public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {

    if (pSceneTouchEvent.getX() > cameraWidth / 2) {
      isAccPressed = false;
      isBrakePressed = false;
      if (!pSceneTouchEvent.isActionUp()
          && isTouchInside(buttonAcc, pSceneTouchEvent.getX(),
              pSceneTouchEvent.getY())) {
        isAccPressed = true;
        buttonAcc.changeState(ButtonSprite.State.PRESSED);
      } else if (!pSceneTouchEvent.isActionUp()
          && isTouchInside(buttonBrake, pSceneTouchEvent.getX(),
              pSceneTouchEvent.getY())) {
        isBrakePressed = true;
        buttonBrake.changeState(ButtonSprite.State.PRESSED);
      }

      if (!isAccPressed)
        buttonAcc.changeState(ButtonSprite.State.NORMAL);

      if (!isBrakePressed)
        buttonBrake.changeState(ButtonSprite.State.NORMAL);
    }

    return false;
  }

  @Override
  public void onResumeGame() {
    if (status == Game.STATUS_LOADING) {
      startCountdown();
    }

    super.onResumeGame();
  }

  private void startCountdown() {
    new CountDownTimer(5000, 1000) {

      public void onTick(long millisUntilFinished) {
        if ((int) (millisUntilFinished / 1000) == 3) {
          countdownText.setText("2 ");
        } else if ((int) (millisUntilFinished / 1000) == 2) {
          countdownText.setText("1 ");
          timerText.setVisible(true);
          buttonAcc.setVisible(true);
          buttonBrake.setVisible(true);
          timerFrame.setVisible(true);
          buttonPause.setVisible(true);
          analogOnScreenControl.setVisible(true);
        } else if ((int) (millisUntilFinished / 1000) == 1) {
          status = Game.STATUS_RUNNING;
          startTime = System.currentTimeMillis();
          stepCount = 0;
          timerText.setVisible(true);
          buttonAcc.setVisible(true);
          buttonBrake.setVisible(true);
          timerFrame.setVisible(true);
          buttonPause.setVisible(true);
          analogOnScreenControl.setVisible(true);
          countdownText.setText("GO!");
        }
      }

      public void onFinish() {
        timerText.setVisible(true);
        buttonAcc.setVisible(true);
        buttonBrake.setVisible(true);
        timerFrame.setVisible(true);
        buttonPause.setVisible(true);
        analogOnScreenControl.setVisible(true);
        countdownText.setVisible(false);
        countdownSprite.setVisible(false);
      }
    }.start();

  }

  @Override
  public void onPauseGame() {
    if (status == Game.STATUS_RUNNING) {
      pauseGame();
    }
    super.onPauseGame();
  }

  public static String getDurationParsed(long millis) {

    long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
    millis -= TimeUnit.MINUTES.toMillis(minutes);
    long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);
    millis -= TimeUnit.SECONDS.toMillis(seconds);

    return String.format("%02d", minutes) + ":"
        + String.format("%02d", seconds) + "." + String.format("%03d", millis);
  }

  class myUpdateHandler implements IUpdateHandler {

    public void onUpdate(float pSecondsElapsed) {

      mCamera.setCenterDirect(bike.getBodySprite().getX() + 200f, bike
          .getBodySprite().getY());

      checkWheelParticleSystem();

      if (status != Game.STATUS_RUNNING)
        return;

      // check bike position to respawn
      if (bikeIsFalling()) {
        respawnBike();
        return;
      }

      bike.balance(analogControllerXValue);

      /*
       * if (buttonAcc.isPressed()) { bike.accelerate(); } else if
       * (buttonBrake.isPressed()) { bike.brake();
       * 
       * } else { bike.stop(); }
       */
      if (isAccPressed) {
        bike.accelerate();
        if (rearWheelContactingGround)
          bike.impulse();
      } else if (rearWheelContactingGround && isBrakePressed) {
        bike.brake();
      } else {
        bike.stop();
      }

      // springs
      bike.springs();

      // update dynamic platforms
      // draw dynamic platforms
      for (TrackDynamicPlatform trackDynamicPlatform : track.trackDynamicPlatforms) {
        if (trackDynamicPlatform.movementType == TrackDynamicPlatform.HORIZONTAL) {
          float newMovement = 0;
          float currentX = trackDynamicPlatform.box.getWorldCenter().x
              * PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;

          if (trackDynamicPlatform.movingTo == TrackDynamicPlatform.MOVING_LEFT) {
            if ((trackDynamicPlatform.position.x - currentX) < trackDynamicPlatform.minMovement) {
              newMovement = -(trackDynamicPlatform.steps);
            } else {
              newMovement = 0;
              trackDynamicPlatform.movingTo = TrackDynamicPlatform.MOVING_RIGHT;
            }
          } else if (trackDynamicPlatform.movingTo == TrackDynamicPlatform.MOVING_RIGHT) {
            if ((currentX - trackDynamicPlatform.position.x) < trackDynamicPlatform.maxMovement) {
              newMovement = trackDynamicPlatform.steps;
            } else {
              newMovement = 0;
              trackDynamicPlatform.movingTo = TrackDynamicPlatform.MOVING_LEFT;
            }
          }

          trackDynamicPlatform.box.setLinearVelocity(newMovement, 0);

        }

        if (trackDynamicPlatform.movementType == TrackDynamicPlatform.VERTICAL) {
          float newMovement = 0;
          float currentY = trackDynamicPlatform.box.getWorldCenter().y
              * PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;

          if (trackDynamicPlatform.movingTo == TrackDynamicPlatform.MOVING_DOWN) {
            if ((trackDynamicPlatform.position.y - currentY) < trackDynamicPlatform.minMovement) {
              newMovement = -(trackDynamicPlatform.steps);
            } else {
              newMovement = 0;
              trackDynamicPlatform.movingTo = TrackDynamicPlatform.MOVING_UP;
            }
          } else if (trackDynamicPlatform.movingTo == TrackDynamicPlatform.MOVING_UP) {
            if ((currentY - trackDynamicPlatform.position.y) < trackDynamicPlatform.maxMovement) {
              newMovement = trackDynamicPlatform.steps;
            } else {
              newMovement = 0;
              trackDynamicPlatform.movingTo = TrackDynamicPlatform.MOVING_DOWN;
            }
          }

          trackDynamicPlatform.box.setLinearVelocity(0, newMovement);
        }

      }

      // update ghost bike
      if (ghostLocalModeEnabled && bestPersonalTrackRecord != null
          && stepCount < bestPersonalTrackRecord.b.bS.size()) {
        BikeStep bStep = bestPersonalTrackRecord.b.bS.get(stepCount);
        ghostPersonalBike.setStep(bStep);
      }
      if (ghostGlobalModeEnabled && bestGlobalTrackRecord != null
          && stepCount < bestGlobalTrackRecord.b.bS.size()) {
        BikeStep bStep = bestGlobalTrackRecord.b.bS.get(stepCount);
        ghostGlobalBike.setStep(bStep);
      }

      // set track record data
      BikeStep bikeStep = new BikeStep();
      bikeStep.bP = new Point((int) bike.getBodySprite().getX(), (int) bike
          .getBodySprite().getY());
      bikeStep.bR = bike.getBodySprite().getRotation();
      bikeStep.rP = new Point((int) bike.getRearWheelSprite().getX(),
          (int) bike.getRearWheelSprite().getY());
      bikeStep.rR = bike.getRearWheelSprite().getRotation();
      bikeStep.fP = new Point((int) bike.getFrontWheelSprite().getX(),
          (int) bike.getFrontWheelSprite().getY());
      bikeStep.fR = bike.getFrontWheelSprite().getRotation();
      currentTrackRecord.b.bS.add(bikeStep);

      String timer = Game.getDurationParsed(System.currentTimeMillis()
          - startTime);
      timerText.setText(timer);

      stepCount++;

    }

    @Override
    public void reset() {
      // TODO Auto-generated method stub

    }
  }

  class myCollidesHandler implements IUpdateHandler {

    public void onUpdate(float pSecondsElapsed) {

      for (TrackZoomPoint zoomPoint : track.trackZoomPoints) {
        if (zoomPoint.zoomEntity.isCulled(mCamera) == false
            && bike.getBodySprite().collidesWith(zoomPoint.zoomEntity)) {
          mCamera.setZoomFactor(zoomPoint.zoomFactor);
          break;
        }
      }

      for (TrackGravityModifier gravityModifier : track.trackGravityModifiers) {
        if (gravityModifier.gravityEntity.isCulled(mCamera) == false
            && bike.getBodySprite().collidesWith(gravityModifier.gravityEntity)) {
          mPhysicsWorld.setGravity(gravityModifier.gravity);
          break;
        }
      }

      for (TrackBooster booster : track.trackBoosters) {
        if (booster.sprite.isCulled(mCamera) == false
            && bike.getBodySprite().collidesWith(booster.sprite)) {
          bike.boost(booster.impulse);
          break;
        }
      }

      for (TrackEventPlatform trackEventPlatform : track.trackEventPlatforms) {
        if (trackEventPlatform.eventPointSprite.isCulled(mCamera) == false
            && bike.getBodySprite().collidesWith(
                trackEventPlatform.eventPointSprite)) {
          if (trackEventPlatform.event == TrackEventPlatform.SET_VISIBLE) {
            trackEventPlatform.line.setVisible(true);
            trackEventPlatform.lineBody.setActive(true);
          } else {
            trackEventPlatform.line.setVisible(false);
            trackEventPlatform.lineBody.setActive(false);
          }
        }
      }

      if (status == Game.STATUS_RUNNING
          && bike.getBodySprite().collidesWith(endCheckpointSprite)) {
        status = Game.STATUS_END;
        currentTrackRecord.t = System.currentTimeMillis() - startTime;

        // set timer
        String timer = Game.getDurationParsed(currentTrackRecord.t);
        timerText.setText(timer);

        if (ghostLocalModeEnabled && bestPersonalTrackRecord != null) {
          if (bestPersonalTrackRecord.t > currentTrackRecord.t) {
            String betterTime = Game
                .getDurationParsed(bestPersonalTrackRecord.t
                    - currentTrackRecord.t);
            ghostTimerText.setColor(Color.GREEN);
            ghostTimerText.setText("- " + betterTime);
          } else {
            String worseTime = Game.getDurationParsed(currentTrackRecord.t
                - bestPersonalTrackRecord.t);
            ghostTimerText.setColor(Color.RED);
            ghostTimerText.setText("+ " + worseTime);
          }
          ghostTimerText.setVisible(true);
        }

        if (bestPersonalTrackRecord == null
            || bestPersonalTrackRecord.t > currentTrackRecord.t) {

          final ScheduledExecutorService worker = Executors
              .newSingleThreadScheduledExecutor();
          Runnable task = new Runnable() {
            public void run() {

              SharedPreferencesStorage.save(currentTrackRecord.b,
                  "trackRecord_" + currentTrackRecord.i + "_"
                      + currentTrackRecord.m + "_steps",
                  getApplicationContext());

              currentTrackRecord.b = null;
              currentTrackRecord.isSynchronized = false;
              SharedPreferencesStorage.save(currentTrackRecord, "trackRecord_"
                  + currentTrackRecord.i + "_" + currentTrackRecord.m,
                  getApplicationContext());
            }
          };
          worker.schedule(task, 10, TimeUnit.MILLISECONDS);
        }

        showScoreModal();
      }

      if (status == Game.STATUS_RUNNING
          && activatedCheckpoints < track.checkpoints.size()
          && bike.getBodySprite().collidesWith(
              track.checkpoints.get(activatedCheckpoints).sprite)) {
        activatedCheckpoints++;
      }

    }

    @Override
    public void reset() {
      // TODO Auto-generated method stub

    }
  }

  @Override
  protected int getLayoutID() {
    return R.layout.activity_game;
  }

  private void checkWheelParticleSystem() {
    if (rearWheelContactingDifference != null) {
      wheelParticleEmitter
          .setCenter(
              bike.getRearWheelSprite().getX()
                  - (rearWheelContactingDifference.x * PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT),
              bike.getRearWheelSprite().getY()
                  - (rearWheelContactingDifference.y * PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT));
    }

    if (rearWheelContactingGround == true
        && bike.getRearWheelBody().getAngularVelocity() < -25) {
      wheelParticleSystem.setParticlesSpawnEnabled(true);

      // depending on gravity set particle velocity
      float maxY = (mPhysicsWorld.getGravity().y < 0) ? 100f : -100f;
      float minY = (mPhysicsWorld.getGravity().y < 0) ? 50f : -50f;

      float maxX = (mPhysicsWorld.getGravity().x < 0) ? 100f : -100f;
      float minX = (mPhysicsWorld.getGravity().x < 0) ? 50f : -50f;

      wheelVelocityParticleInitializer.setVelocity(minX, maxX, minY, maxY);
    } else {

      wheelParticleSystem.setParticlesSpawnEnabled(false);
    }

  }

  public void respawnBike() {
    // redraw track event platforms
    for (TrackEventPlatform trackEventPlatform : track.trackEventPlatforms) {

      if (trackEventPlatform.event == TrackEventPlatform.SET_VISIBLE) {
        trackEventPlatform.line.setVisible(false);
        trackEventPlatform.lineBody.setActive(false);
      } else {
        trackEventPlatform.line.setVisible(true);
        trackEventPlatform.lineBody.setActive(true);
      }
    }

    // redraw tplatforms
    for (TrackTPlatform trackTplatform : track.trackTPlatforms) {
      trackTplatform.joint.getBodyB().applyTorque(trackTplatform.tRotation);
    }

    // redraw track event platforms
    for (TrackEventPlatform trackEventPlatform : track.trackEventPlatforms) {

      if (trackEventPlatform.event == TrackEventPlatform.SET_VISIBLE) {
        trackEventPlatform.line.setVisible(false);
        trackEventPlatform.lineBody.setActive(false);
      } else {
        trackEventPlatform.line.setVisible(true);
        trackEventPlatform.lineBody.setActive(true);
      }
    }

    Point respawnPoint = new Point();
    if (activatedCheckpoints == 0) {
      respawnPoint.x = track.bikeInitialSpawnPosition.x;
      respawnPoint.y = track.bikeInitialSpawnPosition.y;
      mCamera.setZoomFactor(track.initialZoomFactor);
      mPhysicsWorld.setGravity(track.initialGravity);
    } else {
      Checkpoint lastCheckpoint = track.checkpoints
          .get(activatedCheckpoints - 1);
      respawnPoint.x = lastCheckpoint.position.x;
      respawnPoint.y = lastCheckpoint.position.y;

      mCamera
          .setZoomFactor(((lastCheckpoint.zoomFactor == null) ? track.initialZoomFactor
              : lastCheckpoint.zoomFactor));
      mPhysicsWorld
          .setGravity(((lastCheckpoint.gravity == null) ? track.initialGravity
              : lastCheckpoint.gravity));
    }

    bike.respawn(mScene, mPhysicsWorld, respawnPoint,
        getVertexBufferObjectManager(), getTextureManager());
  }

  public boolean bikeIsFalling() {
    if (bike.getBodySprite().getY() < -2000) {
      return true;
    }
    return false;
  }

  public void showScoreModal() {
    runOnUiThread(new Runnable() {

      @Override
      public void run() {
        scoreDialog = new Dialog(Game.this);
        scoreDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        scoreDialog.getWindow().setBackgroundDrawable(
            new ColorDrawable(Color.TRANSPARENT));
        scoreDialog.setContentView(getApplicationContext().getResources()
            .getIdentifier("dialog_score", "layout",
                getApplicationContext().getPackageName()));
        scoreDialog.setCanceledOnTouchOutside(false);
        scoreDialog.setCancelable(false);

        // set up dialog ui
        // Look up the AdView as a resource and load a request.
        AdView adView = (AdView) scoreDialog.findViewById(R.id.adView);
        if (Utils.isFreeVersion) {
          AdRequest adRequest = new AdRequest.Builder().addTestDevice(
              "33B892C281D109B2").build();
          adView.loadAd(adRequest);
        } else {
          adView.setVisibility(View.GONE);
        }

        Button goToMenuButton = (Button) scoreDialog
            .findViewById(R.id.goToMenuButton);
        goToMenuButton.setOnClickListener(new OnClickListener() {

          @Override
          public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), Main.class);
            startActivity(intent);
            finish();
          }
        });

        Button goToTrackSelectionButton = (Button) scoreDialog
            .findViewById(R.id.goToTrackSelectionButton);
        goToTrackSelectionButton.setOnClickListener(new OnClickListener() {

          @Override
          public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(),
                TrackSelection.class);
            intent.putExtra("levelId", level.id);
            intent.putExtra("trackId", track.id);
            startActivity(intent);
            finish();
          }
        });

        Button tryAgainButton = (Button) scoreDialog
            .findViewById(R.id.tryAgainButton);
        tryAgainButton.setOnClickListener(new OnClickListener() {

          @Override
          public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), Game.class);
            intent.putExtra("trackId", track.id);
            intent.putExtra("levelId", level.id);
            startActivity(intent);
            finish();
          }
        });

        // try to get next track
        nextTrack = -1;
        for (int i = 0; i < level.tracks.size(); i++) {
          if (level.tracks.get(i).id == track.id) {
            if (i < (level.tracks.size() - 1)) {
              nextTrack = level.tracks.get(i + 1).id;
              break;
            }
          }
        }

        Button goToNextTrackButton = (Button) scoreDialog
            .findViewById(R.id.goToNextTrackButton);
        if (nextTrack != -1) {
          goToNextTrackButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

              Intent intent = new Intent(getApplicationContext(), Game.class);
              intent.putExtra("levelId", level.id);
              intent.putExtra("trackId", nextTrack);
              startActivity(intent);
              finish();
            }
          });
        } else {
          goToNextTrackButton.setVisibility(View.INVISIBLE);
        }

        // set times
        TextView currentScore = (TextView) scoreDialog
            .findViewById(R.id.currentScore);
        currentScore.setText(Game.getDurationParsed(currentTrackRecord.t));

        TextView track1lightningTime = (TextView) scoreDialog
            .findViewById(R.id.track1lightningTime);
        track1lightningTime.setText(Game
            .getDurationParsed(track.recordOneLightning));

        TextView track2lightningTime = (TextView) scoreDialog
            .findViewById(R.id.track2lightningTime);
        track2lightningTime.setText(Game
            .getDurationParsed(track.recordTwoLightning));

        TextView track3lightningTime = (TextView) scoreDialog
            .findViewById(R.id.track3lightningTime);
        track3lightningTime.setText(Game
            .getDurationParsed(track.recordThreeLightning));

        // check score image
        Integer lightningsScore = 0;
        ImageView currentTimeLightnings = (ImageView) scoreDialog
            .findViewById(R.id.currentTimeLightnings);
        if (currentTrackRecord.t < track.recordThreeLightning) {
          currentTimeLightnings.setImageResource(R.drawable.lightning_3);
          lightningsScore = 3;
        } else if (currentTrackRecord.t < track.recordTwoLightning) {
          currentTimeLightnings.setImageResource(R.drawable.lightning_2);
          lightningsScore = 2;
        } else if (currentTrackRecord.t < track.recordOneLightning) {
          currentTimeLightnings.setImageResource(R.drawable.lightning_1);
          lightningsScore = 1;
        } else {
          currentTimeLightnings.setImageResource(R.drawable.lightning_0);
        }

        // set new scores
        if (lightningsScore > currentTrackLightnings) {
          SharedPreferencesStorage.save(lightningsScore, "trackLightnings_"
              + track.id + "_" + level.id, getApplicationContext());

          // set new level score
          Integer newLevelLightnings = currentLevelLightnings
              + (lightningsScore - currentTrackLightnings);
          SharedPreferencesStorage.save(newLevelLightnings, "levelLightnings_"
              + level.id, getApplicationContext());

          // set new total score
          Integer newTotalLightnings = totalLightnings
              + (lightningsScore - currentTrackLightnings);
          SharedPreferencesStorage.save(newTotalLightnings, "totalLightnings",
              getApplicationContext());
        }

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(scoreDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        scoreDialog.show();
        scoreDialog.getWindow().setAttributes(lp);

        scoreDialog.show();

      }
    });

  }

  @Override
  protected int getRenderSurfaceViewID() {
    return R.id.gameEngineSurfaceView;
  }

  private void pauseGame() {
    if (status == Game.STATUS_RUNNING) {
      status = Game.STATUS_PAUSED;
      mScene.setIgnoreUpdate(true);
      pauseTime = System.currentTimeMillis();
      showPauseDialog();
    } else if (status == Game.STATUS_PAUSED) {
      status = Game.STATUS_RUNNING;
      mScene.setIgnoreUpdate(false);
      startTime += (System.currentTimeMillis() - pauseTime);
      pauseTime = 0;
    }
  }

  private void showPauseDialog() {
    runOnUiThread(new Runnable() {

      @Override
      public void run() {
        pauseDialog = new Dialog(Game.this);
        pauseDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pauseDialog.getWindow().setBackgroundDrawable(
            new ColorDrawable(Color.TRANSPARENT));
        pauseDialog.setContentView(getApplicationContext().getResources()
            .getIdentifier("dialog_pause", "layout",
                getApplicationContext().getPackageName()));
        pauseDialog.setCanceledOnTouchOutside(false);
        pauseDialog.setCancelable(false);

        // set up dialog ui
        // Look up the AdView as a resource and load a request.
        AdView adView = (AdView) pauseDialog.findViewById(R.id.adView);
        if (Utils.isFreeVersion) {
          AdRequest adRequest = new AdRequest.Builder().addTestDevice(
              "33B892C281D109B2").build();
          adView.loadAd(adRequest);
        } else {
          adView.setVisibility(View.GONE);
        }

        Button goToMenuButton = (Button) pauseDialog
            .findViewById(R.id.goToMenuButton);
        goToMenuButton.setOnClickListener(new OnClickListener() {

          @Override
          public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), Main.class);
            startActivity(intent);
            finish();
          }
        });

        Button tryAgainButton = (Button) pauseDialog
            .findViewById(R.id.tryAgainButton);
        tryAgainButton.setOnClickListener(new OnClickListener() {

          @Override
          public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), Game.class);
            intent.putExtra("trackId", track.id);
            intent.putExtra("levelId", level.id);
            startActivity(intent);
            finish();
          }
        });

        Button backToGameButton = (Button) pauseDialog
            .findViewById(R.id.backToGameButton);
        backToGameButton.setOnClickListener(new OnClickListener() {

          @Override
          public void onClick(View v) {
            pauseDialog.dismiss();
            pauseGame();
          }
        });

        Button goToTrackSelectionButton = (Button) pauseDialog
            .findViewById(R.id.goToTrackSelectionButton);
        goToTrackSelectionButton.setOnClickListener(new OnClickListener() {

          @Override
          public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(),
                TrackSelection.class);
            intent.putExtra("levelId", level.id);
            intent.putExtra("trackId", track.id);
            startActivity(intent);
            finish();
          }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(pauseDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        pauseDialog.show();
        pauseDialog.getWindow().setAttributes(lp);

        pauseDialog.show();
      }
    });
  }

  @Override
  public void onBackPressed() {
    if (status == Game.STATUS_RUNNING) {
      pauseGame();
    }
  }
}
