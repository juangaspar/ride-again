package com.jdroidev.rideagain.activities;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Locale;

import org.apache.http.Header;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.jdroidev.rideagain.R;
import com.jdroidev.rideagain.api.ApiConnector;
import com.jdroidev.rideagain.api.SynchronizationResult;
import com.jdroidev.rideagain.game.Level;
import com.jdroidev.rideagain.game.SharedPreferencesStorage;
import com.jdroidev.rideagain.game.Track;
import com.jdroidev.rideagain.game.trackelements.TrackRecord;
import com.jdroidev.rideagain.tracks.LevelsDefinition;
import com.jdroidev.rideagain.utils.Installation;
import com.jdroidev.rideagain.utils.Utils;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class TrackSelection extends FragmentActivity {

	TracksPagerAdapter tracksPagerAdapter;
	static ViewPager viewPager;
	protected int selectedTrackId;
	public static View rootView;
	private static Level level;
	private static int levelId;
	private static ArrayList<Track> tracks;
	private static ProgressDialog pd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_track_selection);
		loadTracks();
		setUpUi();

		// Look up the AdView as a resource and load a request.
		AdView adView = (AdView) this.findViewById(R.id.adView);
		if (Utils.isFreeVersion) {
			AdRequest adRequest = new AdRequest.Builder().addTestDevice(
					"33B892C281D109B2").build();
			adView.loadAd(adRequest);
		} else {
			adView.setVisibility(View.GONE);
		}
	}

	private void loadTracks() {
		levelId = getIntent().getIntExtra("levelId", 1);
		LevelsDefinition levelsDefinition = new LevelsDefinition();

		for (Level levelDef : levelsDefinition.levels) {
			if (levelDef.id == levelId) {
				level = levelDef;
				tracks = levelDef.tracks;
				break;
			}
		}

		// set selected track
		int trackId = getIntent().getIntExtra("trackId", 1);
		selectedTrackId = trackId;

		ImageView leftArrow = (ImageView) findViewById(R.id.leftArrow);
		leftArrow.setVisibility(View.INVISIBLE);
	}

	private void setUpUi() {
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the activity.
		tracksPagerAdapter = new TracksPagerAdapter(getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		viewPager = (ViewPager) findViewById(R.id.tracksPager);
		viewPager.setAdapter(tracksPagerAdapter);

		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				selectedTrackId = tracks.get(arg0).id;

				ImageView leftArrow = (ImageView) findViewById(R.id.leftArrow);
				ImageView rightArrow = (ImageView) findViewById(R.id.rightArrow);

				if (arg0 == 0) {
					leftArrow.setVisibility(View.INVISIBLE);
				} else {
					leftArrow.setVisibility(View.VISIBLE);
				}

				if (arg0 < (tracks.size() - 1)) {
					rightArrow.setVisibility(View.VISIBLE);
				} else {
					rightArrow.setVisibility(View.INVISIBLE);
				}

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});

		// set selected track id
		for (int i = 0; i < tracks.size(); i++) {
			if (tracks.get(i).id == selectedTrackId) {
				viewPager.setCurrentItem(i);
			}
		}

		ImageView backButton = (ImageView) findViewById(R.id.backButton);
		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(),
						LevelSelection.class);
				intent.putExtra("levelId", levelId);
				startActivity(intent);
				finish();
			}
		});

		ImageView leftArrow = (ImageView) findViewById(R.id.leftArrow);
		leftArrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
			}
		});

		ImageView rightArrow = (ImageView) findViewById(R.id.rightArrow);
		rightArrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
			}
		});

		Button closeRankingButton = (Button) findViewById(R.id.closeRankingButton);
		closeRankingButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				View trackRankingContainer = findViewById(R.id.trackRankingContainer);
				trackRankingContainer.setVisibility(View.GONE);
			}
		});

	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class TracksPagerAdapter extends FragmentPagerAdapter {

		public TracksPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a PlaceholderFragment (defined as a static inner class
			// below).
			return PlaceholderFragment.newInstance(position);
		}

		@Override
		public int getCount() {
			return tracks.size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return (position + 1) + ". "
					+ getResources().getString(tracks.get(0).nameResource);
		}

	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_TRACK_POSITION = "track_position";

		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static PlaceholderFragment newInstance(int position) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_TRACK_POSITION, position);
			fragment.setArguments(args);
			return fragment;
		}

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			rootView = inflater.inflate(R.layout.fragment_track_selection,
					container, false);
			int trackPosition = getArguments().getInt(ARG_TRACK_POSITION);
			final Track track = tracks.get(trackPosition);
			Integer currentLightnings = SharedPreferencesStorage.get(
					"trackLightnings_" + track.id + "_" + levelId,
					Integer.class, getActivity().getApplicationContext());
			currentLightnings = (currentLightnings == null) ? 0
					: currentLightnings;

			// set track name
			TextView trackTitle = (TextView) rootView
					.findViewById(R.id.trackTitle);
			trackTitle.setText((trackPosition + 1) + ". "
					+ getResources().getString(track.nameResource));

			// track personal best time
			TrackRecord personalTrackRecord = SharedPreferencesStorage.get(
					"trackRecord_" + track.id + "_" + level.mode,
					TrackRecord.class, getActivity().getApplicationContext());
			if (personalTrackRecord != null) {
				TextView personalBestTimeTextView = (TextView) rootView
						.findViewById(R.id.personalBestTime);
				personalBestTimeTextView.setText(Game
						.getDurationParsed(personalTrackRecord.t));
			}

			// track global best time
			TrackRecord globalTrackRecord = SharedPreferencesStorage.get(
					"globalTrackRecord_" + track.id + "_" + level.mode,
					TrackRecord.class, getActivity().getApplicationContext());
			if (globalTrackRecord != null) {
				TextView globalBestTimeTextView = (TextView) rootView
						.findViewById(R.id.absoluteBestTime);
				globalBestTimeTextView.setText(Game
						.getDurationParsed(globalTrackRecord.t));
			}

			// set track lightnings image
			ImageView lightningImage = (ImageView) rootView
					.findViewById(R.id.lightningImage);
			int imageResource = R.drawable.lightning_0;

			switch (currentLightnings) {
			case 3:
				imageResource = R.drawable.lightning_3;
				break;
			case 2:
				imageResource = R.drawable.lightning_2;
				break;
			case 1:
				imageResource = R.drawable.lightning_1;
				break;
			}
			lightningImage.setImageResource(imageResource);

			Button goToTrackButton = (Button) rootView
					.findViewById(R.id.goToTrackButton);
			goToTrackButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(getActivity()
							.getApplicationContext(), Game.class);
					intent.putExtra("trackId", track.id);
					intent.putExtra("levelId", levelId);
					startActivity(intent);
					getActivity().finish();

				}
			});

			Button viewRankingButton = (Button) rootView
					.findViewById(R.id.viewRankingButton);
			viewRankingButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// show dialog
					pd = ProgressDialog
							.show(getActivity(), null,
									getString(R.string.synchronizing_data),
									true, false);
					showRanking(track.id);
				}
			});

			return rootView;
		}

		protected void showRanking(int id) {
			String trackId = "trackRecord_" + id + "_" + level.mode;
			ApiConnector.post("getTrackRanking",
					new AsyncHttpResponseHandler() {
						@Override
						public void onSuccess(String response) {
							drawRanking(response);
							pd.dismiss();
						}

						@Override
						public void onFailure(int arg0, Header[] arg1,
								byte[] arg2, Throwable arg3) {
							pd.dismiss();
						}
					}, SharedPreferencesStorage.objectToJson(trackId));
		}

		protected void drawRanking(String response) {
			// parse response
			SynchronizationResult result = SharedPreferencesStorage
					.JsonToObject(response, SynchronizationResult.class);

			String uuid = Installation.id(getActivity());

			for (int i = 0; i < 5; i++) {
				// get record
				TrackRecord trackRecord = (i < result.trackRecords.size()) ? result.trackRecords
						.get(i) : null;

				// get row fields
				TextView posField = (TextView) getActivity().findViewById(
						getActivity().getResources().getIdentifier(
								"pos_" + (i + 1), "id",
								getActivity().getPackageName()));
				TextView riderField = (TextView) getActivity().findViewById(
						getActivity().getResources().getIdentifier(
								"rider_" + (i + 1), "id",
								getActivity().getPackageName()));
				TextView timeField = (TextView) getActivity().findViewById(
						getActivity().getResources().getIdentifier(
								"time_" + (i + 1), "id",
								getActivity().getPackageName()));
				ImageView flagField = (ImageView) getActivity().findViewById(
						getActivity().getResources().getIdentifier(
								"flag_" + (i + 1), "id",
								getActivity().getPackageName()));
				// set text fields color
				riderField.setTextColor(Color.WHITE);
				timeField.setTextColor(Color.WHITE);
				posField.setTextColor(Color.WHITE);

				if (trackRecord == null) {
					riderField.setText("---");
					timeField.setText("--:--.---");
					flagField.setImageDrawable(null);
				} else {
					riderField.setText(trackRecord.r.name);
					timeField.setText(Game.getDurationParsed(trackRecord.t));

					String drawableName = "flag_"
							+ trackRecord.r.flag.toLowerCase(Locale.ENGLISH);
					flagField.setImageResource(getResId(drawableName));

					if (trackRecord.u.equals(uuid)) {
						riderField.setTextColor(Color.parseColor("#FFDA00"));
						timeField.setTextColor(Color.parseColor("#FFDA00"));
						posField.setTextColor(Color.parseColor("#FFDA00"));
					}
				}
			}

			// set ranking container visible
			View trackRankingContainer = getActivity().findViewById(
					R.id.trackRankingContainer);
			trackRankingContainer.setOnTouchListener(new OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					return true;
				}
			});
			trackRankingContainer.setVisibility(View.VISIBLE);

		}
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(getApplicationContext(), Main.class);
		startActivity(intent);
		finish();
	}

	private static int getResId(String drawableName) {

		try {
			Class<com.jdroidev.rideagain.R.drawable> res = R.drawable.class;
			Field field = res.getField(drawableName);
			int drawableId = field.getInt(null);
			return drawableId;
		} catch (Exception e) {
			Log.e("COUNTRYPICKER", "Failure to get drawable id.", e);
		}
		return -1;
	}
}
