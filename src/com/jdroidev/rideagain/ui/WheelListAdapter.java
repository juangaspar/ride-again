package com.jdroidev.rideagain.ui;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.jdroidev.rideagain.R;
import com.jdroidev.rideagain.game.Wheel;

public class WheelListAdapter extends BaseAdapter {

  private Context context;
  ArrayList<Wheel> wheels;
  LayoutInflater inflater;

  /**
   * Constructor
   * 
   * @param context
   * @param countries
   */
  public WheelListAdapter(Context context, ArrayList<Wheel> wheels) {
    super();
    this.context = context;
    this.wheels = wheels;
    inflater = (LayoutInflater) this.context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  @Override
  public int getCount() {
    // TODO Auto-generated method stub
    return wheels.size();
  }

  @Override
  public Object getItem(int arg0) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public long getItemId(int arg0) {
    // TODO Auto-generated method stub
    return 0;
  }

  /**
   * Return row for each country
   */
  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View cellView = convertView;
    Cell cell;
    Wheel wheel = wheels.get(position);

    if (convertView == null) {
      cell = new Cell();
      cellView = inflater.inflate(R.layout.row, null);
      cell.imageView = (ImageView) cellView.findViewById(R.id.row_icon);
      cellView.setTag(cell);
    } else {
      cell = (Cell) cellView.getTag();
    }

    // Load drawable dynamically from country code
    cell.imageView.setImageResource(context.getResources().getIdentifier(
        wheel.design, "drawable", context.getPackageName()));
    return cellView;
  }

  /**
   * Holder for the cell
   * 
   */
  static class Cell {
    public ImageView imageView;
  }

}