package com.jdroidev.rideagain.ui;

/**
 * Inform the client which country has been selected
 * 
 */
public interface WheelPickerListener {
	public void onSelectWheel(Integer id);
}
