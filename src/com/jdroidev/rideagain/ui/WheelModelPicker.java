package com.jdroidev.rideagain.ui;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.countrypicker.Country;
import com.jdroidev.rideagain.R;
import com.jdroidev.rideagain.game.Wheel;

public class WheelModelPicker extends DialogFragment implements
    Comparator<Country> {
  /**
   * View components
   */
  private ListView wheelListView;

  /**
   * Adapter for the listview
   */
  private WheelListAdapter adapter;

  /**
   * Hold all countries, sorted by country name
   */
  private ArrayList<Wheel> allWheelModelsList;

  /**
   * Listener to which country user selected
   */
  private WheelPickerListener listener;

  /**
   * Set listener
   * 
   * @param listener
   */
  public void setListener(WheelPickerListener listener) {
    this.listener = listener;
  }

  /**
   * Get all countries with code and name from res/raw/countries.json
   * 
   * @return
   */
  private ArrayList<Wheel> getAllWheels() {
    if (allWheelModelsList == null) {
      try {
        allWheelModelsList = Wheel.loadWheels();

        // Return
        return allWheelModelsList;

      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return null;
  }

  /**
   * To support show as dialog
   * 
   * @param dialogTitle
   * @return
   */
  public static WheelModelPicker newInstance(String dialogTitle) {
    WheelModelPicker picker = new WheelModelPicker();
    Bundle bundle = new Bundle();
    bundle.putString("dialogTitle", dialogTitle);
    picker.setArguments(bundle);
    return picker;
  }

  /**
   * Create view
   */
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate view
    View view = inflater.inflate(R.layout.country_picker, null);

    // Get countries from the json
    getAllWheels();

    // Set dialog title if show as dialog
    Bundle args = getArguments();
    if (args != null) {
      String dialogTitle = args.getString("dialogTitle");
      getDialog().setTitle(dialogTitle);

      int width = getResources().getDimensionPixelSize(R.dimen.cp_dialog_width);
      int height = getResources().getDimensionPixelSize(
          R.dimen.cp_dialog_height);
      getDialog().getWindow().setLayout(width, height);
    }

    wheelListView = (ListView) view.findViewById(R.id.country_picker_listview);

    // Set adapter
    adapter = new WheelListAdapter(getActivity(), allWheelModelsList);
    wheelListView.setAdapter(adapter);

    // Inform listener
    wheelListView.setOnItemClickListener(new OnItemClickListener() {

      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position,
          long id) {
        if (listener != null) {
          Wheel wheel = allWheelModelsList.get(position);
          listener.onSelectWheel(wheel.id);
        }
      }
    });

    return view;
  }

  /**
   * Support sorting the countries list
   */
  @Override
  public int compare(Country lhs, Country rhs) {
    return lhs.getName().compareTo(rhs.getName());
  }

}
