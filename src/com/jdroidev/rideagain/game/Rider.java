package com.jdroidev.rideagain.game;

public class Rider {
	public String name;
	public String flag;
	public Integer wheelId;

	public Rider(String name, String flag, int wheelId) {
		this.name = name;
		this.flag = flag;
		this.wheelId = wheelId;
	}
}
