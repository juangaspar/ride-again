package com.jdroidev.rideagain.game;

import java.util.ArrayList;

import android.graphics.Point;

import com.badlogic.gdx.math.Vector2;
import com.jdroidev.rideagain.game.trackelements.Checkpoint;
import com.jdroidev.rideagain.game.trackelements.TrackBooster;
import com.jdroidev.rideagain.game.trackelements.TrackBox;
import com.jdroidev.rideagain.game.trackelements.TrackCircle;
import com.jdroidev.rideagain.game.trackelements.TrackDynamicPlatform;
import com.jdroidev.rideagain.game.trackelements.TrackEventPlatform;
import com.jdroidev.rideagain.game.trackelements.TrackGravityModifier;
import com.jdroidev.rideagain.game.trackelements.TrackLine;
import com.jdroidev.rideagain.game.trackelements.TrackLinesGroup;
import com.jdroidev.rideagain.game.trackelements.TrackPipe;
import com.jdroidev.rideagain.game.trackelements.TrackTPlatform;
import com.jdroidev.rideagain.game.trackelements.TrackZoomPoint;

public abstract class Track {

  public static String cB = "#000000";
  
  public int id;
  public int nameResource;
  public Point bikeInitialSpawnPosition;
  public float initialZoomFactor;
  public ArrayList<TrackLine> trackLines = new ArrayList<TrackLine>();
  public ArrayList<TrackLinesGroup> trackLinesGroups = new ArrayList<TrackLinesGroup>();
  public ArrayList<TrackBox> trackBoxes = new ArrayList<TrackBox>();
  public ArrayList<TrackCircle> trackCircles = new ArrayList<TrackCircle>();
  public ArrayList<TrackTPlatform> trackTPlatforms = new ArrayList<TrackTPlatform>();
  public ArrayList<TrackDynamicPlatform> trackDynamicPlatforms = new ArrayList<TrackDynamicPlatform>();
  public ArrayList<Checkpoint> checkpoints = new ArrayList<Checkpoint>();
  public ArrayList<TrackZoomPoint> trackZoomPoints = new ArrayList<TrackZoomPoint>();
  public ArrayList<TrackGravityModifier> trackGravityModifiers = new ArrayList<TrackGravityModifier>();
  public ArrayList<TrackBooster> trackBoosters = new ArrayList<TrackBooster>();
  public ArrayList<TrackEventPlatform> trackEventPlatforms = new ArrayList<TrackEventPlatform>();
  public ArrayList<TrackPipe> trackPipes = new ArrayList<TrackPipe>();
  public Checkpoint endCheckpoint;
  public long recordOneLightning;
  public long recordTwoLightning;
  public long recordThreeLightning;
  public Vector2 initialGravity;

  protected abstract void load();

  protected Point p(int x, int y) {
    return new Point(x, y);
  }
}
