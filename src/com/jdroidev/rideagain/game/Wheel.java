package com.jdroidev.rideagain.game;

import java.util.ArrayList;

public class Wheel {
  public Integer id;
  public String design;
  public Float maxVelocity;
  public float maxTorque;

  public Wheel(Integer id, String design, float maxVelocity, float maxTorque) {
    this.id = id;
    this.design = design;
    this.maxVelocity = maxVelocity;
    this.maxTorque = maxTorque;
  }

  public static ArrayList<Wheel> loadWheels() {
    ArrayList<Wheel> wheelsList = new ArrayList<Wheel>();
    wheelsList.add(new Wheel(1, "wheel_1", 15, 100));
    wheelsList.add(new Wheel(2, "wheel_2", 30, 150));
    wheelsList.add(new Wheel(3, "wheel_3", 60, 200));
    wheelsList.add(new Wheel(4, "wheel_4", 120, 250));
    return wheelsList;
  }

  public static Wheel load(Integer wheelId) {
    ArrayList<Wheel> wheelsList = Wheel.loadWheels();

    for (Wheel wheel : wheelsList) {
      if (wheelId == wheel.id)
        return wheel;
    }

    return null;
  }
}
