package com.jdroidev.rideagain.game;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.font.FontManager;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.align.HorizontalAlign;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;

public class GhostBike {

  private Context context;

  // Textures objects
  private BitmapTextureAtlas bikeBodyTexture;
  private TextureRegion bikeBodyTextureRegion;
  private BitmapTextureAtlas wheelTexture;
  private TextureRegion wheelTextureRegion;

  private Sprite riderBodySprite;
  private ITextureRegion riderBodyTextureRegion;
  private BitmapTextureAtlas riderBodyTexture;

  private Sprite rearWheelSprite;
  private Sprite bikeBodySprite;

  private Sprite frontWheelSprite;

  private Boolean isGlobal;

  private Sprite markerSprite;
  private ITextureRegion markerTextureRegion;
  private BitmapTextureAtlas markerTexture;

  private Sprite flagSprite;
  private ITextureRegion flagTextureRegion;
  private BitmapTextureAtlas flagTexture;
  private Rider rider;
  private Font fontGhostName;
  private Text riderName;
  private Wheel wheel;

  public GhostBike(Context context, Rider rider, Boolean isGlobal) {
    this.context = context;
    this.rider = rider;
    this.wheel = Wheel.load(rider.wheelId);
    this.isGlobal = isGlobal;

  }

  public void initialize(Scene scene, Point spawnPosition,
      VertexBufferObjectManager vertexBufferObjectManager,
      TextureManager textureManager, FontManager fontManager) {
    loadResources(textureManager, fontManager);

    // rider body
    riderBodySprite = new Sprite(spawnPosition.x + 2, spawnPosition.y + 8,
        riderBodyTextureRegion, vertexBufferObjectManager);
    riderBodySprite.setAlpha(0.3f);
    riderBodySprite.setZIndex(2);
    riderBodySprite.setVisible(false);
    scene.attachChild(riderBodySprite);

    // bike body
    bikeBodySprite = new Sprite(spawnPosition.x, spawnPosition.y,
        bikeBodyTextureRegion, vertexBufferObjectManager);
    bikeBodySprite.setAlpha(0.3f);
    bikeBodySprite.setVisible(false);
    scene.attachChild(bikeBodySprite);

    // rear wheel
    rearWheelSprite = new Sprite(spawnPosition.x - 30f, spawnPosition.y - 23,
        wheelTextureRegion, vertexBufferObjectManager);
    rearWheelSprite.setAlpha(0.3f);
    rearWheelSprite.setVisible(false);
    scene.attachChild(rearWheelSprite);

    // front wheel
    frontWheelSprite = new Sprite(spawnPosition.x + 30, spawnPosition.y - 23,
        wheelTextureRegion, vertexBufferObjectManager);
    frontWheelSprite.setAlpha(0.3f);
    frontWheelSprite.setVisible(false);
    scene.attachChild(frontWheelSprite);

    // marker
    markerSprite = new Sprite(spawnPosition.x, spawnPosition.y + 50,
        markerTextureRegion, vertexBufferObjectManager);
    markerSprite.setAlpha(0.6f);
    markerSprite.setVisible(false);
    scene.attachChild(markerSprite);

    if (isGlobal) {

      // flag
      flagSprite = new Sprite(spawnPosition.x - 20, spawnPosition.y + 80,
          flagTextureRegion, vertexBufferObjectManager);
      flagSprite.setScale(0.7f);
      flagSprite.setAlpha(0.7f);
      flagSprite.setVisible(false);
      scene.attachChild(flagSprite);

      // rider name
      TextOptions textOptions = new TextOptions();
      textOptions.setHorizontalAlign(HorizontalAlign.CENTER);
      riderName = new Text(spawnPosition.x + 10, spawnPosition.y + 80,
          fontGhostName, rider.name, rider.name.length(), textOptions,
          vertexBufferObjectManager);
      riderName.setVisible(false);
      riderName.setAlpha(0.7f);
      scene.attachChild(riderName);
    }

  }

  private void loadResources(TextureManager textureManager,
      FontManager fontManager) {
    riderBodyTexture = new BitmapTextureAtlas(textureManager, 30, 51,
        TextureOptions.BILINEAR);
    riderBodyTextureRegion = BitmapTextureAtlasTextureRegionFactory
        .createFromAsset(riderBodyTexture, context, "rider_body_full_min.png",
            0, 0);
    riderBodyTexture.load();

    bikeBodyTexture = new BitmapTextureAtlas(textureManager, 70, 24,
        TextureOptions.BILINEAR);
    bikeBodyTextureRegion = BitmapTextureAtlasTextureRegionFactory
        .createFromAsset(bikeBodyTexture, context, "moto_body.png", 0, 0);
    bikeBodyTexture.load();

    wheelTexture = new BitmapTextureAtlas(textureManager, 32, 32,
        TextureOptions.BILINEAR);
    wheelTextureRegion = BitmapTextureAtlasTextureRegionFactory
        .createFromAsset(wheelTexture, context, wheel.design + ".png", 0, 0);
    wheelTexture.load();

    markerTexture = new BitmapTextureAtlas(textureManager, 24, 24,
        TextureOptions.BILINEAR);
    markerTextureRegion = BitmapTextureAtlasTextureRegionFactory
        .createFromAsset(markerTexture, context,
            ((isGlobal) ? "marker_global_ghost.png"
                : "marker_personal_ghost.png"), 0, 0);
    markerTexture.load();

    if (isGlobal) {
      flagTexture = new BitmapTextureAtlas(textureManager, 48, 48,
          TextureOptions.BILINEAR);
      flagTextureRegion = BitmapTextureAtlasTextureRegionFactory
          .createFromAsset(flagTexture, context, "flags/flag_" + rider.flag
              + ".png", 0, 0);
      flagTexture.load();

      // Load our font
      fontGhostName = FontFactory.create(fontManager, textureManager, 256, 256,
          Typeface.create(Typeface.DEFAULT, Typeface.NORMAL), 26f, true,
          Color.BLACK);
      fontGhostName.load();
    }
  }

  public void setStep(BikeStep bikeStep) {
    bikeBodySprite.setVisible(true);
    rearWheelSprite.setVisible(true);
    frontWheelSprite.setVisible(true);
    riderBodySprite.setVisible(true);
    markerSprite.setVisible(true);

    riderBodySprite.setPosition(bikeStep.bP.x, bikeStep.bP.y + 8);
    riderBodySprite.setRotation(bikeStep.bR);
    bikeBodySprite.setPosition(bikeStep.bP.x, bikeStep.bP.y);
    bikeBodySprite.setRotation(bikeStep.bR);
    rearWheelSprite.setPosition(bikeStep.rP.x, bikeStep.rP.y);
    rearWheelSprite.setRotation(bikeStep.rR);
    frontWheelSprite.setPosition(bikeStep.fP.x, bikeStep.fP.y);
    frontWheelSprite.setRotation(bikeStep.fR);
    markerSprite.setPosition(bikeStep.bP.x, bikeStep.bP.y + 50);

    if (isGlobal) {
      flagSprite.setVisible(true);
      riderName.setVisible(true);
      flagSprite.setPosition(bikeStep.bP.x - 20, bikeStep.bP.y + 80);
      riderName.setPosition(flagSprite.getX() + (flagSprite.getWidth() / 2)
          + (riderName.getWidth() / 2), bikeStep.bP.y + 80);
    }

  }

}
