package com.jdroidev.rideagain.game;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

public class SharedPreferencesStorage {

	public static String objectToJson(Object object) {
		Gson gson = new Gson();
		return gson.toJson(object);
	}

	public static <T> T JsonToObject(String json, Class<T> objectClass) {
		Gson gson = new Gson();
		return gson.fromJson(json, objectClass);
	}

	public static void save(Object object, String key, Context context) {
		SharedPreferences params = context.getSharedPreferences("shared_data",
				0);
		SharedPreferences.Editor paramsEditor = params.edit();
		paramsEditor.remove(key);
		paramsEditor.putString(key, objectToJson(object));

		// Commit the edits!
		paramsEditor.commit();

	}

	public static <T> T get(String key, Class<T> objectClass, Context context) {
		SharedPreferences params = context.getSharedPreferences("shared_data",
				0);
		String objectJson = params.getString(key, null);
		if (objectJson == null)
			return null;

		return JsonToObject(objectJson, objectClass);

	}

	public static void delete(String key, Context context) {
		SharedPreferences params = context.getSharedPreferences("shared_data",
				0);
		SharedPreferences.Editor paramsEditor = params.edit();
		paramsEditor.remove(key);

		// Commit the edits!
		paramsEditor.commit();
	}
}
