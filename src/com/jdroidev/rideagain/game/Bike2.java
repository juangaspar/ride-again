package com.jdroidev.rideagain.game;

import static org.andengine.extension.physics.box2d.util.constants.PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.Context;
import android.graphics.Point;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJoint;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;

public class Bike2 {

	// ===========================================================
	// Constants
	// ===========================================================
	public static final short CATEGORYBIT_NO = 0;
	public static final short CATEGORYBIT_WALL = 1;
	public static final short CATEGORYBIT_BOX = 2;
	public static final short CATEGORYBIT_CIRCLE = 4;
	public static final short MASKBITS_WALL = CATEGORYBIT_WALL
			+ CATEGORYBIT_BOX + CATEGORYBIT_CIRCLE;
	public static final short MASKBITS_BOX = CATEGORYBIT_WALL + CATEGORYBIT_BOX; // Missing:
	// CATEGORYBIT_CIRCLE
	public static final short MASKBITS_CIRCLE = CATEGORYBIT_WALL
			+ CATEGORYBIT_CIRCLE; // Missing: CATEGORYBIT_BOX
	public static final short MASKBITS_NOTHING = 0; // Missing: all
	public static final short MASKBITS_ONLY_WALL = CATEGORYBIT_WALL; // Missing:

	// wall
	public static final FixtureDef WALL_FIXTURE_DEF = PhysicsFactory
			.createFixtureDef(0, 0.2f, 1f, false, CATEGORYBIT_WALL,
					MASKBITS_WALL, (short) 0);
	public static final FixtureDef BOX_FIXTURE_DEF = PhysicsFactory
			.createFixtureDef(1, 0.5f, 0.5f, false, CATEGORYBIT_BOX,
					MASKBITS_BOX, (short) 0);
	public static final FixtureDef CIRCLE_FIXTURE_DEF = PhysicsFactory
			.createFixtureDef(1, 0.5f, 0.5f, false, CATEGORYBIT_CIRCLE,
					MASKBITS_CIRCLE, (short) 0);
	public static final FixtureDef NO_FIXTURE_DEF = PhysicsFactory
			.createFixtureDef(1, 0.5f, 0.5f, false, CATEGORYBIT_NO,
					MASKBITS_NOTHING, (short) 0);
	public static final FixtureDef ONLY_WALL_FIXTURE_DEF = PhysicsFactory
			.createFixtureDef(1, 0.2f, 0.9f, false, CATEGORYBIT_CIRCLE,
					MASKBITS_ONLY_WALL, (short) -1);

	private Context context;

	// Physics objects
	private Body bikeBody;
	private Body rearWheelBody;
	private Body rearConnectionBody;
	private PrismaticJointDef rearSpringDef;
	private PrismaticJoint rearSpring;
	private RevoluteJointDef rearMotorDef;
	private RevoluteJoint rearMotor;
	private Body frontWheelBody;
	private Body frontConnectionBody;
	private PrismaticJointDef frontSpringDef;
	private PrismaticJoint frontSpring;
	private RevoluteJointDef frontMotorDef;
	private RevoluteJoint frontMotor;

	// Textures objects
	private BitmapTextureAtlas bikeBodyTexture;
	private TextureRegion bikeBodyTextureRegion;
	private BitmapTextureAtlas wheelTexture;
	private TextureRegion wheelTextureRegion;

	private Sprite rearWheelSprite;
	private Sprite bikeBodySprite;
	private Sprite frontWheelSprite;
	private String wheelDesign;
	private Sprite riderBodySprite;
	private ITextureRegion riderBodyTextureRegion;
	private BitmapTextureAtlas riderBodyTexture;

	private Sprite riderHeadSprite;
	private ITextureRegion riderHeadTextureRegion;
	private BitmapTextureAtlas riderHeadTexture;

	private Body riderBody;
	public RevoluteJoint riderBikeJoint;
	private Body riderHeadBody;
	public RevoluteJoint riderHeadJoint;
	private Rectangle carConn2Layer;
	private Rectangle rearConnectionLayer;

	public Bike2(Context context, String wheelDesign) {
		this.context = context;
		this.wheelDesign = wheelDesign;
	}

	private void loadResources(TextureManager textureManager) {

		riderHeadTexture = new BitmapTextureAtlas(textureManager, 18, 17,
				TextureOptions.BILINEAR);
		riderHeadTextureRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(riderHeadTexture, context, "rider_head.png",
						0, 0);
		riderHeadTexture.load();

		riderBodyTexture = new BitmapTextureAtlas(textureManager, 30, 34,
				TextureOptions.BILINEAR);
		riderBodyTextureRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(riderBodyTexture, context,
						"rider_body_min.png", 0, 0);
		riderBodyTexture.load();

		bikeBodyTexture = new BitmapTextureAtlas(textureManager, 70, 24,
				TextureOptions.BILINEAR);
		bikeBodyTextureRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(bikeBodyTexture, context, "moto_body.png", 0,
						0);
		bikeBodyTexture.load();

		wheelTexture = new BitmapTextureAtlas(textureManager, 32, 32,
				TextureOptions.BILINEAR);
		wheelTextureRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(wheelTexture, context, wheelDesign + ".png",
						0, 0);
		wheelTexture.load();
	}

	public void initialize(Scene scene, PhysicsWorld physicsWorld,
			Point spawnPosition,
			VertexBufferObjectManager vertexBufferObjectManager,
			TextureManager textureManager, Boolean loadResources) {
		if (loadResources)
			loadResources(textureManager);

		// rider head
		riderHeadSprite = new Sprite(spawnPosition.x - 4, spawnPosition.y + 25,
				riderHeadTextureRegion, vertexBufferObjectManager);
		riderHeadSprite.setZIndex(3);
		scene.attachChild(riderHeadSprite);

		riderHeadBody = PhysicsFactory.createBoxBody(physicsWorld,
				riderHeadSprite, BodyType.DynamicBody, PhysicsFactory
						.createFixtureDef(1, 0.2f, 0.9f, false,
								CATEGORYBIT_CIRCLE, MASKBITS_ONLY_WALL,
								(short) -1));
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(
				riderHeadSprite, riderHeadBody, true, true));

		// rider body
		riderBodySprite = new Sprite(spawnPosition.x, spawnPosition.y,
				riderBodyTextureRegion, vertexBufferObjectManager);
		riderBodySprite.setZIndex(2);
		scene.attachChild(riderBodySprite);

		riderBody = PhysicsFactory.createBoxBody(physicsWorld, riderBodySprite,
				BodyType.DynamicBody, PhysicsFactory.createFixtureDef(1, 0.2f,
						0.9f, false, CATEGORYBIT_CIRCLE, MASKBITS_ONLY_WALL,
						(short) -1));

		// attach rider head to rider body
		RevoluteJointDef riderHeadJointDef = new RevoluteJointDef();
		riderHeadJointDef.initialize(riderHeadBody, riderBody,
				riderHeadBody.getWorldCenter());
		riderHeadJointDef.lowerAngle = 0;
		riderHeadJointDef.upperAngle = 0;
		riderHeadJointDef.enableLimit = true;
		riderHeadJointDef.collideConnected = false;
		riderHeadJoint = (RevoluteJoint) physicsWorld
				.createJoint(riderHeadJointDef);

		MassData md = new MassData();
		md.I = 1;
		md.mass = 5;
		riderBody.setMassData(md);
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(
				riderBodySprite, riderBody, true, true));

		// bike body
		bikeBodySprite = new Sprite(spawnPosition.x, spawnPosition.y,
				bikeBodyTextureRegion, vertexBufferObjectManager);
		scene.attachChild(bikeBodySprite);
		bikeBody = PhysicsFactory.createBoxBody(physicsWorld, bikeBodySprite,
				BodyType.DynamicBody, ONLY_WALL_FIXTURE_DEF);

		md = new MassData();
		md.I = 10;
		md.mass = 10;
		bikeBody.setMassData(md);
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(
				bikeBodySprite, bikeBody, true, true));

		// attach rider to bike body
		RevoluteJointDef riderBikeJointDef = new RevoluteJointDef();
		riderBikeJointDef.initialize(bikeBody, riderBody,
				bikeBody.getWorldCenter());
		riderBikeJointDef.lowerAngle = 0;
		riderBikeJointDef.upperAngle = 0;
		riderBikeJointDef.enableLimit = true;
		riderBikeJointDef.collideConnected = false;
		riderBikeJoint = (RevoluteJoint) physicsWorld
				.createJoint(riderBikeJointDef);

		// rear wheel
		rearWheelSprite = new Sprite(spawnPosition.x - 30f,
				spawnPosition.y - 23, wheelTextureRegion,
				vertexBufferObjectManager);

		scene.attachChild(rearWheelSprite);
		rearWheelBody = PhysicsFactory.createCircleBody(physicsWorld,
				rearWheelSprite, BodyType.DynamicBody, ONLY_WALL_FIXTURE_DEF);
		md = new MassData();
		md.I = 1;
		md.mass = 15;

		rearWheelBody.setMassData(md);
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(
				rearWheelSprite, rearWheelBody, true, true));

		// rear connection
		rearConnectionLayer = new Rectangle(spawnPosition.x - 20f,
				spawnPosition.y, 30f, 30f, vertexBufferObjectManager);
		rearConnectionLayer.setVisible(false);
		scene.attachChild(rearConnectionLayer);
		rearConnectionBody = PhysicsFactory.createCircleBody(physicsWorld,
				rearConnectionLayer, BodyType.DynamicBody, NO_FIXTURE_DEF);
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(
				rearConnectionLayer, rearConnectionBody, true, true));

		// rear spring
		rearSpringDef = new PrismaticJointDef();
		Float a1 = (float) (-(Math.cos(Math.PI / 3)));
		Float a2 = (float) ((Math.sin(Math.PI / 3)));
		rearSpringDef.initialize(bikeBody, rearConnectionBody, new Vector2(
				bikeBody.getWorldCenter().x + 0 / PIXEL_TO_METER_RATIO_DEFAULT,
				bikeBody.getWorldCenter().y), new Vector2(a1, a2));
		rearSpringDef.lowerTranslation = (float) 0.2f
				/ PIXEL_TO_METER_RATIO_DEFAULT;
		rearSpringDef.upperTranslation = (float) 0.2f
				/ PIXEL_TO_METER_RATIO_DEFAULT;

		rearSpringDef.enableLimit = true;
		rearSpringDef.enableMotor = false;
		rearSpring = (PrismaticJoint) physicsWorld.createJoint(rearSpringDef);

		// rear motor
		rearMotorDef = new RevoluteJointDef();
		rearMotorDef.initialize(rearConnectionBody, rearWheelBody,
				rearWheelBody.getWorldCenter());
		rearMotorDef.enableMotor = true;
		rearMotorDef.motorSpeed = 0;
		rearMotorDef.maxMotorTorque = 200;
		rearMotor = (RevoluteJoint) physicsWorld.createJoint(rearMotorDef);

		// front wheel
		frontWheelSprite = new Sprite(spawnPosition.x + 30,
				spawnPosition.y - 23, wheelTextureRegion,
				vertexBufferObjectManager);

		scene.attachChild(frontWheelSprite);
		frontWheelBody = PhysicsFactory.createCircleBody(physicsWorld,
				frontWheelSprite, BodyType.DynamicBody, ONLY_WALL_FIXTURE_DEF);
		md = new MassData();
		md.I = 1;
		md.mass = 15;

		frontWheelBody.setMassData(md);
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(
				frontWheelSprite, frontWheelBody, true, true));

		// carConn 2
		carConn2Layer = new Rectangle(spawnPosition.x + 20f, spawnPosition.y,
				30f, 30f, vertexBufferObjectManager);

		carConn2Layer.setVisible(false);
		scene.attachChild(carConn2Layer);
		frontConnectionBody = PhysicsFactory.createCircleBody(physicsWorld,
				carConn2Layer, BodyType.DynamicBody, NO_FIXTURE_DEF);
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(
				carConn2Layer, frontConnectionBody, true, true));

		// spring 2
		frontSpringDef = new PrismaticJointDef();
		Float b1 = (float) (-(Math.cos(Math.PI / 3)));
		Float b2 = (float) ((Math.sin(Math.PI / 3)));
		frontSpringDef.initialize(bikeBody, frontConnectionBody, new Vector2(
				bikeBody.getWorldCenter().x + 0 / PIXEL_TO_METER_RATIO_DEFAULT,
				bikeBody.getWorldCenter().y), new Vector2(b1, b2));
		frontSpringDef.lowerTranslation = (float) 0.2f
				/ PIXEL_TO_METER_RATIO_DEFAULT;
		frontSpringDef.upperTranslation = (float) 0.2f
				/ PIXEL_TO_METER_RATIO_DEFAULT;

		frontSpringDef.enableLimit = true;
		frontSpringDef.enableMotor = false;
		frontSpring = (PrismaticJoint) physicsWorld.createJoint(frontSpringDef);

		// motor 2
		frontMotorDef = new RevoluteJointDef();
		frontMotorDef.initialize(frontConnectionBody, frontWheelBody,
				frontWheelBody.getWorldCenter());
		frontMotor = (RevoluteJoint) physicsWorld.createJoint(frontMotorDef);

		scene.sortChildren();
	}

	public void accelerate() {
		rearMotor.enableMotor(true);
		rearMotor.setMotorSpeed(-30);
		rearMotor.setMaxMotorTorque(750f);

		frontMotor.enableMotor(true);
		frontMotor.setMotorSpeed(0);
		frontMotor.setMaxMotorTorque(0);
	}

	public void brake() {
		if (rearMotor.getMotorSpeed() > 0) {
			// brake
			rearMotor.enableMotor(true);
			rearMotor.setMotorSpeed(0);
			rearMotor.setMaxMotorTorque(0);

			frontMotor.enableMotor(true);
			frontMotor.setMotorSpeed(0);
			frontMotor.setMaxMotorTorque(500f);
		} else {
			// reverse
			rearMotor.enableMotor(true);
			rearMotor.setMotorSpeed(25);
			rearMotor.setMaxMotorTorque(500f);

			frontMotor.enableMotor(true);
			frontMotor.setMotorSpeed(0);
			frontMotor.setMaxMotorTorque(0);
		}

	}

	public void stop() {
		rearMotor.enableMotor(true);
		rearMotor.setMotorSpeed(0);
		rearMotor.setMaxMotorTorque(0);

		frontMotor.enableMotor(true);
		frontMotor.setMotorSpeed(0);
		frontMotor.setMaxMotorTorque(0);
	}

	public void balance(float torque) {
		bikeBody.applyTorque(torque);
	}

	public void boost(Vector2 impulse) {
		bikeBody.applyLinearImpulse(impulse, bikeBody.getWorldCenter());
	}

	public void springs() {
		/*
		 * rearSpring.setMaxMotorForce((float) (40 + Math.abs(400 * Math.pow(
		 * rearSpring.getJointTranslation(), 2)))); rearSpring
		 * .setMotorSpeed((float) ((rearSpring.getMotorSpeed() - 10 * rearSpring
		 * .getJointTranslation()) * 0.4));
		 * 
		 * frontSpring.setMaxMotorForce((float) (40 + Math.abs(400 * Math.pow(
		 * frontSpring.getJointTranslation(), 2)))); frontSpring
		 * .setMotorSpeed((float) ((frontSpring.getMotorSpeed() - 10 *
		 * frontSpring .getJointTranslation()) * 0.4));
		 */
	}

	public Sprite getBodySprite() {
		return this.bikeBodySprite;
	}

	public Sprite getRearWheelSprite() {
		return this.rearWheelSprite;
	}

	public Sprite getFrontWheelSprite() {
		return this.frontWheelSprite;
	}

	public void respawn(Scene scene, PhysicsWorld physicsWorld,
			Point spawnPosition,
			VertexBufferObjectManager vertexBufferObjectManager,
			TextureManager textureManager) {

		physicsWorld.unregisterPhysicsConnector(physicsWorld
				.getPhysicsConnectorManager().findPhysicsConnectorByShape(
						riderHeadSprite));
		physicsWorld.destroyBody(riderHeadBody);
		scene.detachChild(riderHeadSprite);

		physicsWorld.unregisterPhysicsConnector(physicsWorld
				.getPhysicsConnectorManager().findPhysicsConnectorByShape(
						riderBodySprite));
		physicsWorld.destroyBody(riderBody);
		scene.detachChild(riderBodySprite);

		physicsWorld.unregisterPhysicsConnector(physicsWorld
				.getPhysicsConnectorManager().findPhysicsConnectorByShape(
						bikeBodySprite));
		physicsWorld.destroyBody(bikeBody);
		scene.detachChild(bikeBodySprite);

		physicsWorld.unregisterPhysicsConnector(physicsWorld
				.getPhysicsConnectorManager().findPhysicsConnectorByShape(
						rearWheelSprite));
		physicsWorld.destroyBody(rearWheelBody);
		scene.detachChild(rearWheelSprite);

		physicsWorld.unregisterPhysicsConnector(physicsWorld
				.getPhysicsConnectorManager().findPhysicsConnectorByShape(
						frontWheelSprite));
		physicsWorld.destroyBody(frontWheelBody);
		scene.detachChild(frontWheelSprite);

		physicsWorld.unregisterPhysicsConnector(physicsWorld
				.getPhysicsConnectorManager().findPhysicsConnectorByShape(
						carConn2Layer));
		physicsWorld.destroyBody(frontConnectionBody);
		scene.detachChild(carConn2Layer);

		physicsWorld.unregisterPhysicsConnector(physicsWorld
				.getPhysicsConnectorManager().findPhysicsConnectorByShape(
						rearConnectionLayer));
		physicsWorld.destroyBody(rearConnectionBody);
		scene.detachChild(rearConnectionLayer);

		this.initialize(scene, physicsWorld, spawnPosition,
				vertexBufferObjectManager, textureManager, false);
	}

	public Body getRiderHeadBody() {
		return riderHeadBody;
	}

	public Body getRearWheelBody() {
		return rearWheelBody;
	}

}
