package com.jdroidev.rideagain.game.trackelements;

import android.graphics.Point;

import com.badlogic.gdx.physics.box2d.Body;

public class TrackDynamicPlatform {

	public final static int HORIZONTAL = 1;
	public final static int VERTICAL = 2;

	public final static int MOVING_UP = 1;
	public final static int MOVING_DOWN = 2;
	public final static int MOVING_LEFT = 3;
	public final static int MOVING_RIGHT = 4;

	public Point position;
	public float longitude;
	public float width;
	public String color;
	public int movementType;
	public float maxMovement;
	public float minMovement;
	public float steps;

	// used in game
	public Body box;
	public int movingTo;

	public TrackDynamicPlatform(Point position, float longitude, float width,
			String color, int movementType, float maxMovement,
			float minMovement, int movingTo, int steps) {
		this.position = position;
		this.longitude = longitude;
		this.width = width;
		this.color = color;
		this.movementType = movementType;
		this.maxMovement = maxMovement;
		this.minMovement = minMovement;
		this.movingTo = movingTo;
		this.steps = steps;
	}

}
