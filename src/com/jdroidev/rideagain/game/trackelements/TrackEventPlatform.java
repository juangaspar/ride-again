package com.jdroidev.rideagain.game.trackelements;

import org.andengine.entity.primitive.Line;
import org.andengine.entity.sprite.Sprite;

import android.graphics.Point;

import com.badlogic.gdx.physics.box2d.Body;

public class TrackEventPlatform extends TrackLine {
	public final static int SET_VISIBLE = 1;
	public final static int SET_HIDDEN = 2;

	public Point eventPoint;
	public int event;

	// used in game
	public Line line;
	public Body lineBody;
	public Sprite eventPointSprite;

	public TrackEventPlatform(Point initialPoint, Point endPoint, float width,
			String color, Point eventPoint, int event) {
		super(initialPoint, endPoint, width, color);
		this.eventPoint = eventPoint;
		this.event = event;
	}
}
