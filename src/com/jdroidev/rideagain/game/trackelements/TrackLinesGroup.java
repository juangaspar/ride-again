package com.jdroidev.rideagain.game.trackelements;

import java.util.ArrayList;

import android.graphics.Point;

public class TrackLinesGroup {

  public ArrayList<Point> initialPoints = new ArrayList<Point>();
  public ArrayList<Point> endPoints = new ArrayList<Point>();
  public float width;
  public String color;

  public TrackLinesGroup(ArrayList<Point> segmentPoints, float width,
      String color) {
    this.width = width;
    this.color = color;

    initialPoints.add(segmentPoints.remove(0));

    Point endPoint = segmentPoints.remove(segmentPoints.size() - 1);

    for (Point point : segmentPoints) {
      initialPoints.add(point);
      endPoints.add(point);
    }

    endPoints.add(endPoint);
  }

}
