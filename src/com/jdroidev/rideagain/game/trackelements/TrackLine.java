package com.jdroidev.rideagain.game.trackelements;

import android.graphics.Point;

public class TrackLine {

	public TrackLine(Point initialPoint, Point endPoint, float width,
			String color) {
		this.initialPoint = initialPoint;
		this.endPoint = endPoint;
		this.width = width;
		this.color = color;
	}

	public Point initialPoint;
	public Point endPoint;
	public float width;
	public String color;
}
