package com.jdroidev.rideagain.game.trackelements;

import android.graphics.Point;

public class TrackBox {

	public Point position;
	public float width;
	public float height;
	public String color;

	public TrackBox(Point position, float width, float height, String color) {
		this.position = position;
		this.width = width;
		this.height = height;
		this.color = color;
	}
}
