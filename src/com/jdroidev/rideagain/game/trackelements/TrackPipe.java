package com.jdroidev.rideagain.game.trackelements;

import java.util.ArrayList;

import android.graphics.Point;

public class TrackPipe {

	public final static int DOWN_RIGHT = 1;
	public final static int DOWN_LEFT = 2;
	public final static int UP_RIGHT = 3;
	public final static int UP_LEFT = 4;

	public ArrayList<Point> initialPoints = new ArrayList<Point>();
	public ArrayList<Point> endPoints = new ArrayList<Point>();
	public float width;
	public String color;

	public TrackPipe(Point pointA, Point pointB, Point pointC,
			Integer numberOfSegments, int cornerPosition, float width,
			String color) {

		this.width = width;
		this.color = color;

		ArrayList<Point> abPoints = new ArrayList<Point>();
		ArrayList<Point> bcPoints = new ArrayList<Point>();
		ArrayList<Point> segmentPoints = new ArrayList<Point>();
		int xSegmentDistance = 0;
		int ySegmentDistance = 0;

		if (cornerPosition == TrackPipe.DOWN_RIGHT
				|| cornerPosition == TrackPipe.UP_RIGHT) {
			xSegmentDistance = Math.abs((pointB.x - pointA.x)
					/ numberOfSegments);
			ySegmentDistance = Math.abs((pointC.y - pointB.y)
					/ numberOfSegments);
		} else if (cornerPosition == TrackPipe.DOWN_LEFT
				|| cornerPosition == TrackPipe.UP_LEFT) {
			xSegmentDistance = Math.abs((pointC.x - pointB.x)
					/ numberOfSegments);
			ySegmentDistance = Math.abs((pointA.y - pointB.y)
					/ numberOfSegments);
		}

		for (int i = 1; i < numberOfSegments; i++) {
			Integer x1Point = 0;
			Integer y1Point = 0;
			Integer y2Point = 0;
			Integer x2Point = 0;

			if (cornerPosition == TrackPipe.DOWN_RIGHT) {
				x1Point = pointA.x + (xSegmentDistance * i);
				y1Point = pointA.y;
				y2Point = pointB.y + (ySegmentDistance * i);
				x2Point = pointC.x;
			} else if (cornerPosition == TrackPipe.DOWN_LEFT) {
				x1Point = pointA.x;
				y1Point = pointA.y - (ySegmentDistance * i);
				x2Point = pointB.x + (xSegmentDistance * i);
				y2Point = pointB.y;
			} else if (cornerPosition == TrackPipe.UP_LEFT) {
				x1Point = pointA.x;
				y1Point = pointA.y + (ySegmentDistance * i);
				x2Point = pointB.x + (xSegmentDistance * i);
				y2Point = pointB.y;
			} else if (cornerPosition == TrackPipe.UP_RIGHT) {
				x1Point = pointA.x + (xSegmentDistance * i);
				y1Point = pointA.y;
				x2Point = pointB.x;
				y2Point = pointB.y - (ySegmentDistance * i);
			}

			abPoints.add(new Point(x1Point, y1Point));
			bcPoints.add(new Point(x2Point, y2Point));
		}

		for (int i = 0; i < abPoints.size(); i++) {
			Point segmentPoint = null;

			segmentPoint = getIntersectionPoint(new Point(pointA.x, pointA.y),
					new Point(bcPoints.get(i).x, bcPoints.get(i).y), new Point(
							abPoints.get(i).x, abPoints.get(i).y), new Point(
							pointC.x, pointC.y));

			segmentPoints.add(segmentPoint);
		}

		initialPoints.add(pointA);

		for (Point point : segmentPoints) {
			initialPoints.add(point);
			endPoints.add(point);
		}

		endPoints.add(pointC);
	}

	// calculates intersection and checks for parallel lines.
	// also checks that the intersection point is actually on
	// the line segment p1-p2
	private Point getIntersectionPoint(Point p1, Point p2, Point p3, Point p4) {
		float xD1, yD1, xD2, yD2, xD3, yD3;
		float dot;
		double deg;
		double len2;
		double len1;
		double segmentLen1;
		double segmentLen2;
		float ua, ub, div;

		// calculate differences
		xD1 = p2.x - p1.x;
		xD2 = p4.x - p3.x;
		yD1 = p2.y - p1.y;
		yD2 = p4.y - p3.y;
		xD3 = p1.x - p3.x;
		yD3 = p1.y - p3.y;

		// calculate the lengths of the two lines
		len1 = Math.sqrt(xD1 * xD1 + yD1 * yD1);
		len2 = Math.sqrt(xD2 * xD2 + yD2 * yD2);

		// calculate angle between the two lines.
		dot = (xD1 * xD2 + yD1 * yD2); // dot product
		deg = dot / (len1 * len2);

		// if abs(angle)==1 then the lines are parallell,
		// so no intersection is possible
		if (Math.abs(deg) == 1)
			return null;

		// find intersection Pt between two lines
		Point pt = new Point(0, 0);
		div = yD2 * xD1 - xD2 * yD1;
		ua = (xD2 * yD3 - yD2 * xD3) / div;
		ub = (xD1 * yD3 - yD1 * xD3) / div;
		pt.x = (int) (p1.x + ua * xD1);
		pt.y = (int) (p1.y + ua * yD1);

		// calculate the combined length of the two segments
		// between Pt-p1 and Pt-p2
		xD1 = pt.x - p1.x;
		xD2 = pt.x - p2.x;
		yD1 = pt.y - p1.y;
		yD2 = pt.y - p2.y;
		segmentLen1 = Math.sqrt(xD1 * xD1 + yD1 * yD1)
				+ Math.sqrt(xD2 * xD2 + yD2 * yD2);

		// calculate the combined length of the two segments
		// between Pt-p3 and Pt-p4
		xD1 = pt.x - p3.x;
		xD2 = pt.x - p4.x;
		yD1 = pt.y - p3.y;
		yD2 = pt.y - p4.y;
		segmentLen2 = Math.sqrt(xD1 * xD1 + yD1 * yD1)
				+ Math.sqrt(xD2 * xD2 + yD2 * yD2);

		// return the valid intersection
		return pt;
	}

}
