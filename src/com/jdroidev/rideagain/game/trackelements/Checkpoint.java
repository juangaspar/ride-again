package com.jdroidev.rideagain.game.trackelements;

import org.andengine.entity.sprite.Sprite;

import com.badlogic.gdx.math.Vector2;

import android.graphics.Point;

public class Checkpoint {
  public static final int COLOR_BLUE = 1;
  public static final int COLOR_ORANGE = 2;

  public Point position;
  public int color;
  public Float zoomFactor;
  public Vector2 gravity;

  // used during game
  public Sprite sprite;

  public Checkpoint(Point position, int color, Float zoomFactor, Vector2 gravity) {
    this.position = position;
    this.color = color;
    this.zoomFactor = zoomFactor;
    this.gravity = gravity;
  }
}
