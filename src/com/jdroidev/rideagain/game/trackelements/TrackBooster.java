package com.jdroidev.rideagain.game.trackelements;

import org.andengine.entity.sprite.Sprite;

import android.graphics.Point;

import com.badlogic.gdx.math.Vector2;

public class TrackBooster {

	public Point position;
	public Vector2 impulse;

	// used in game
	public Sprite sprite;

	public TrackBooster(Point position, Vector2 impulse) {
		this.position = position;
		this.impulse = impulse;
	}
}
