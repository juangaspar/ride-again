package com.jdroidev.rideagain.game.trackelements;

import com.jdroidev.rideagain.game.BikeStepsPackage;
import com.jdroidev.rideagain.game.Rider;

public class TrackRecord {

	// TRACK ID
	public int i;
	
	// TRACK LEVEL
	public int l;

	// TRACK MODE
	public int m;

	// TOTAL TIME
	public long t;
	
	// TOTAL uuid
	public String u;

	// checkpoint times
	public BikeStepsPackage b;
	public Rider r;
	public Boolean isSynchronized;
}
