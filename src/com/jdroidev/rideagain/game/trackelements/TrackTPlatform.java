package com.jdroidev.rideagain.game.trackelements;

import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint;

import android.graphics.Point;

public class TrackTPlatform {
  public Point basePosition;
  public int height;
  public int tLongitude;
  public float tOffset;
  public float tRotation;
  public String tColor;
  public float baseWidth;
  public String baseColor;
  public float tUpperAngle;
  public float tLowerAngle;

  // used in gameplay
  public RevoluteJoint joint;

  public TrackTPlatform(Point basePosition, int height, int tLongitude,
      float tRotation, float tOffset, float baseWidth, float tUpperAngle,
      float tLowerAngle, String baseColor, String tColor) {
    this.basePosition = basePosition;
    this.height = height;
    this.tLongitude = tLongitude;
    this.tOffset = tOffset;
    this.baseWidth = baseWidth;
    this.baseColor = baseColor;
    this.tColor = tColor;
    this.tRotation = tRotation;
    this.tUpperAngle = tUpperAngle;
    this.tLowerAngle = tLowerAngle;
  }

}
