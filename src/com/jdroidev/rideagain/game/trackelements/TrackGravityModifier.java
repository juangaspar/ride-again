package com.jdroidev.rideagain.game.trackelements;

import org.andengine.entity.sprite.Sprite;

import android.graphics.Point;

import com.badlogic.gdx.math.Vector2;

public class TrackGravityModifier {

	public Point position;
	public Vector2 gravity;

	// used in game
	public Sprite gravityEntity;

	public TrackGravityModifier(Point position, Vector2 gravity) {
		this.position = position;
		this.gravity = gravity;
	}

}
