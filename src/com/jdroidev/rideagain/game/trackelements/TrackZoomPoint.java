package com.jdroidev.rideagain.game.trackelements;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;

import android.graphics.Point;

public class TrackZoomPoint {

	public Point position;
	public float zoomFactor;

	// used in game
	public Sprite zoomEntity;

	public TrackZoomPoint(Point position, float zoomFactor) {
		this.position = position;
		this.zoomFactor = zoomFactor;
	}

}
