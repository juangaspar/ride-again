package com.jdroidev.rideagain.game;

import java.util.ArrayList;

public abstract class Level {
	/**
	 * Track modes
	 */
	public static final int MODE_NORMAL = 1;

	public int id;
	public int nameResource;
	public int mode;
	public int unlockLightnings;
	public int totalLightnings;
	public int currentLightnings;
	public ArrayList<Track> tracks = new ArrayList<Track>();

	protected abstract void load();
}
