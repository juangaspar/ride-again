package com.jdroidev.rideagain.game;

import android.graphics.Point;

public class BikeStep {
	//bike position
	public Point bP;
	
	//rear wheel position
	public Point rP;
	
	//front wheel position
	public Point fP;
	
	//body rotation
	public float bR;
	
	//rear wheel rotation
	public float rR;
	
	//front wheel rotation
	public float fR;
}
