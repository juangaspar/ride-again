package com.jdroidev.rideagain.api;

import android.os.RemoteException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class ApiConnector {
	private static final String BASE_URL = "http://ns368250.ip-94-23-32.eu/rideagain/index.php";

	private static AsyncHttpClient client = new AsyncHttpClient();

	public static void post(String method,
			AsyncHttpResponseHandler responseHandler, Object... args) {
		RequestParams reObject = buildAsyncParamsRequest(method, args);
		client.post(BASE_URL, reObject, responseHandler);
	}

	/**
	 * Create the POST name-value pairs. JSON-encode the arguments.
	 * 
	 * @param address
	 * @return
	 * @throws RemoteException
	 */
	private static RequestParams buildAsyncParamsRequest(String method,
			Object... params) {

		RequestParams outParams = new RequestParams();
		outParams.put("method", method);
		Gson gson = new GsonBuilder().serializeNulls().create();
		;
		outParams.put("arguments", gson.toJson(params));
		outParams.put("return_format", "json");
		return outParams;
	}
}