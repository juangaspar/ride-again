package com.jdroidev.rideagain.tracks;

import com.jdroidev.rideagain.R;
import com.jdroidev.rideagain.game.Level;

public class Level_1 extends Level {

  public Level_1() {
    load();
  }

  @Override
  protected void load() {
    id = 1;
    mode = Level.MODE_NORMAL;
    nameResource = R.string.level_1;
    unlockLightnings = 0;
    totalLightnings = 9;

    tracks.add(new Track_1());
    tracks.add(new Track_2());
    tracks.add(new Track_3());
    tracks.add(new Track_4());
    tracks.add(new Track_5());
    tracks.add(new Track_6());
  }

}
