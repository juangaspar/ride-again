package com.jdroidev.rideagain.tracks;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import android.graphics.Point;
import android.hardware.SensorManager;

import com.badlogic.gdx.math.Vector2;
import com.jdroidev.rideagain.R;
import com.jdroidev.rideagain.game.Track;
import com.jdroidev.rideagain.game.trackelements.Checkpoint;
import com.jdroidev.rideagain.game.trackelements.TrackBooster;
import com.jdroidev.rideagain.game.trackelements.TrackDynamicPlatform;
import com.jdroidev.rideagain.game.trackelements.TrackEventPlatform;
import com.jdroidev.rideagain.game.trackelements.TrackLine;
import com.jdroidev.rideagain.game.trackelements.TrackLinesGroup;
import com.jdroidev.rideagain.game.trackelements.TrackPipe;

public class Track_8 extends Track {
  public Track_8() {
    load();
  }

  protected void load() {
    this.id = 8;
    this.nameResource = R.string.track_4;
    this.initialZoomFactor = 0.5f;
    this.initialGravity = new Vector2(0f, -SensorManager.GRAVITY_EARTH);
    this.bikeInitialSpawnPosition = p(100, 1300);

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(0, 1200));
        add(p(0, 1000));
        add(p(140, 1000));
        add(p(400, 1200));
        add(p(1400, 400));
        add(p(1800, 400));
      }
    }, 10, Track.cB));

    this.trackBoosters.add(new TrackBooster(p(1750, 430), new Vector2(
        SensorManager.GRAVITY_EARTH * 0.2f, SensorManager.GRAVITY_EARTH * 5)));

    this.trackLines
        .add(new TrackLine(p(2400, 700), p(4000, 700), 10, "#000000"));

    this.trackBoosters.add(new TrackBooster(p(3950, 730), new Vector2(
        SensorManager.GRAVITY_EARTH * 0.2f, SensorManager.GRAVITY_EARTH * 3)));

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(4400, 800));
        add(p(5800, 1400));
        add(p(7000, 1400));
      }
    }, 10, Track.cB));

    this.trackBoosters.add(new TrackBooster(p(6950, 1430), new Vector2(
        SensorManager.GRAVITY_EARTH * 0.2f, SensorManager.GRAVITY_EARTH * 5)));

    this.trackLines.add(new TrackLine(p(7400, 1600), p(8600, 1600), 10,
        "#000000"));
    
    this.trackDynamicPlatforms.add(new TrackDynamicPlatform(p(8600, 1590), 400,
        20, Track.cB, TrackDynamicPlatform.VERTICAL, 400, 0,
        TrackDynamicPlatform.MOVING_DOWN, 3));

    this.endCheckpoint = new Checkpoint(p(9600, 50), Checkpoint.COLOR_ORANGE,
        null, null);

    this.recordOneLightning = TimeUnit.SECONDS.toMillis(15);
    this.recordTwoLightning = TimeUnit.SECONDS.toMillis(10);
    this.recordThreeLightning = TimeUnit.SECONDS.toMillis(8);
  }
}
