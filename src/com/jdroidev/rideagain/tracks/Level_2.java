package com.jdroidev.rideagain.tracks;

import com.jdroidev.rideagain.R;
import com.jdroidev.rideagain.game.Level;

public class Level_2 extends Level {

  public Level_2() {
    load();
  }

  @Override
  protected void load() {
    id = 2;
    mode = Level.MODE_NORMAL;
    nameResource = R.string.level_2;
    unlockLightnings = 0;
    totalLightnings = 9;

    tracks.add(new Track_7());
    tracks.add(new Track_8());
  }

}
