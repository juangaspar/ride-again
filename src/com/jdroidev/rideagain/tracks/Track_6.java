package com.jdroidev.rideagain.tracks;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import android.graphics.Point;
import android.hardware.SensorManager;

import com.badlogic.gdx.math.Vector2;
import com.jdroidev.rideagain.R;
import com.jdroidev.rideagain.game.Track;
import com.jdroidev.rideagain.game.trackelements.Checkpoint;
import com.jdroidev.rideagain.game.trackelements.TrackBox;
import com.jdroidev.rideagain.game.trackelements.TrackCircle;
import com.jdroidev.rideagain.game.trackelements.TrackDynamicPlatform;
import com.jdroidev.rideagain.game.trackelements.TrackEventPlatform;
import com.jdroidev.rideagain.game.trackelements.TrackLine;
import com.jdroidev.rideagain.game.trackelements.TrackLinesGroup;
import com.jdroidev.rideagain.game.trackelements.TrackPipe;

public class Track_6 extends Track {
  public Track_6() {
    load();
  }

  protected void load() {
    this.id = 6;
    this.nameResource = R.string.track_4;
    this.initialZoomFactor = 0.5f;
    this.initialGravity = new Vector2(0f, -SensorManager.GRAVITY_EARTH);
    this.bikeInitialSpawnPosition = p(100, 2300);

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(0, 2200));
        add(p(0, 2000));
        add(p(140, 2000));
        add(p(800, 1200));
        add(p(1200, 1000));
        add(p(1400, 1000));
      }
    }, 10, Track.cB));

    this.trackPipes.add(new TrackPipe(p(1400, 1000), p(1800, 1000), p(1800,
        1400), 10, TrackPipe.DOWN_RIGHT, 10, "#000000"));

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(1800, 1400));
        add(p(2500, 700));
        add(p(3000, 500));
        add(p(3200, 500));
      }
    }, 10, Track.cB));

    this.trackPipes.add(new TrackPipe(p(3200, 500), p(3800, 500),
        p(3800, 1100), 10, TrackPipe.DOWN_RIGHT, 10, "#000000"));

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(3800, 1100));
        add(p(4500, 400));
        add(p(5000, 200));
        add(p(5200, 200));
      }
    }, 10, Track.cB));

    this.trackPipes.add(new TrackPipe(p(5200, 200), p(5800, 200), p(5800, 800),
        10, TrackPipe.DOWN_RIGHT, 10, "#000000"));

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(5800, 800));
        add(p(6500, 100));
        add(p(7000, -100));
        add(p(7200, -100));
      }
    }, 10, Track.cB));

    this.trackPipes.add(new TrackPipe(p(7200, -100), p(7800, -100),
        p(7800, 500), 10, TrackPipe.DOWN_RIGHT, 10, "#000000"));

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(7800, 500));
        add(p(8500, -200));
        add(p(9000, -400));
        add(p(9200, -400));
      }
    }, 10, Track.cB));

    this.trackPipes.add(new TrackPipe(p(9200, -400), p(9800, -400),
        p(9800, 200), 10, TrackPipe.DOWN_RIGHT, 10, "#000000"));

    this.trackLines.add(new TrackLine(p(9800, 200), p(10600, 200), 10,
        "#000000"));

    this.endCheckpoint = new Checkpoint(p(10400, 250), Checkpoint.COLOR_ORANGE,
        null, null);

    this.recordOneLightning = TimeUnit.SECONDS.toMillis(15);
    this.recordTwoLightning = TimeUnit.SECONDS.toMillis(10);
    this.recordThreeLightning = TimeUnit.SECONDS.toMillis(8);
  }
}
