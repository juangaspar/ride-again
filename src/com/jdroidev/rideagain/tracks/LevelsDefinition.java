package com.jdroidev.rideagain.tracks;

import java.util.ArrayList;

import com.jdroidev.rideagain.game.Level;

public class LevelsDefinition {
  public ArrayList<Level> levels = new ArrayList<Level>();

  public LevelsDefinition() {
    levels.add(new Level_1());
    levels.add(new Level_2());
  }
}
