package com.jdroidev.rideagain.tracks;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import android.graphics.Point;
import android.hardware.SensorManager;

import com.badlogic.gdx.math.Vector2;
import com.jdroidev.rideagain.R;
import com.jdroidev.rideagain.game.Track;
import com.jdroidev.rideagain.game.trackelements.Checkpoint;
import com.jdroidev.rideagain.game.trackelements.TrackBox;
import com.jdroidev.rideagain.game.trackelements.TrackCircle;
import com.jdroidev.rideagain.game.trackelements.TrackDynamicPlatform;
import com.jdroidev.rideagain.game.trackelements.TrackEventPlatform;
import com.jdroidev.rideagain.game.trackelements.TrackLine;
import com.jdroidev.rideagain.game.trackelements.TrackLinesGroup;

public class Track_4 extends Track {
  public Track_4() {
    load();
  }

  protected void load() {
    this.id = 4;
    this.nameResource = R.string.track_4;
    this.initialZoomFactor = 0.6f;
    this.initialGravity = new Vector2(0f, -SensorManager.GRAVITY_EARTH);
    this.bikeInitialSpawnPosition = p(100, 200);

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(0, 200));
        add(p(0, 0));
        add(p(200, 0));
      }
    }, 10, Track.cB));

    this.trackCircles.add(new TrackCircle(p(210, 0)));
    this.trackCircles.add(new TrackCircle(p(240, 15)));
    this.trackCircles.add(new TrackCircle(p(270, 30)));
    this.trackCircles.add(new TrackCircle(p(300, 45)));
    this.trackCircles.add(new TrackCircle(p(330, 60)));
    this.trackCircles.add(new TrackCircle(p(360, 75)));
    this.trackCircles.add(new TrackCircle(p(390, 90)));
    this.trackCircles.add(new TrackCircle(p(420, 105)));
    this.trackCircles.add(new TrackCircle(p(450, 120)));
    this.trackCircles.add(new TrackCircle(p(480, 135)));
    this.trackCircles.add(new TrackCircle(p(510, 150)));
    this.trackCircles.add(new TrackCircle(p(540, 135)));
    this.trackCircles.add(new TrackCircle(p(570, 120)));
    this.trackCircles.add(new TrackCircle(p(600, 105)));
    this.trackCircles.add(new TrackCircle(p(630, 90)));
    this.trackCircles.add(new TrackCircle(p(660, 75)));
    this.trackCircles.add(new TrackCircle(p(690, 60)));
    this.trackCircles.add(new TrackCircle(p(720, 45)));
    this.trackCircles.add(new TrackCircle(p(750, 30)));
    this.trackCircles.add(new TrackCircle(p(780, 15)));
    this.trackCircles.add(new TrackCircle(p(810, 0)));

    this.trackEventPlatforms.add(new TrackEventPlatform(p(820, 0), p(900, 0),
        10, Track.cB, p(810, 30), TrackEventPlatform.SET_VISIBLE));
    this.trackEventPlatforms.add(new TrackEventPlatform(p(900, 0), p(1000, 0),
        10, Track.cB, p(830, 30), TrackEventPlatform.SET_VISIBLE));
    this.trackEventPlatforms.add(new TrackEventPlatform(p(1000, 0), p(1100, 0),
        10, Track.cB, p(930, 30), TrackEventPlatform.SET_VISIBLE));
    this.trackEventPlatforms.add(new TrackEventPlatform(p(1100, 0), p(1200, 0),
        10, Track.cB, p(1030, 30), TrackEventPlatform.SET_VISIBLE));
    this.trackEventPlatforms.add(new TrackEventPlatform(p(1200, 0), p(1300, 0),
        10, Track.cB, p(1130, 30), TrackEventPlatform.SET_VISIBLE));
    this.trackEventPlatforms.add(new TrackEventPlatform(p(1300, 0), p(1400, 0),
        10, Track.cB, p(1230, 30), TrackEventPlatform.SET_VISIBLE));
    this.trackEventPlatforms.add(new TrackEventPlatform(p(1400, 0), p(1500, 0),
        10, Track.cB, p(1330, 30), TrackEventPlatform.SET_VISIBLE));
    this.trackEventPlatforms.add(new TrackEventPlatform(p(1500, 0), p(1600, 0),
        10, Track.cB, p(1430, 30), TrackEventPlatform.SET_VISIBLE));
    this.trackEventPlatforms.add(new TrackEventPlatform(p(1600, 0), p(1700, 0),
        10, Track.cB, p(1530, 30), TrackEventPlatform.SET_VISIBLE));

    this.trackLines.add(new TrackLine(p(1600, -200), p(2400, 0), 10, Track.cB));
    this.checkpoints.add(new Checkpoint(p(2000, -50), Checkpoint.COLOR_BLUE,
        null, null));

    this.trackCircles.add(new TrackCircle(p(2410, 0)));
    this.trackCircles.add(new TrackCircle(p(2440, 15)));
    this.trackCircles.add(new TrackCircle(p(2470, 30)));
    this.trackCircles.add(new TrackCircle(p(2500, 45)));
    this.trackCircles.add(new TrackCircle(p(2530, 60)));
    this.trackCircles.add(new TrackCircle(p(2560, 75)));
    this.trackCircles.add(new TrackCircle(p(2590, 90)));
    this.trackCircles.add(new TrackCircle(p(2620, 105)));
    this.trackCircles.add(new TrackCircle(p(2650, 120)));
    this.trackCircles.add(new TrackCircle(p(2680, 135)));
    this.trackCircles.add(new TrackCircle(p(2710, 150)));
    this.trackCircles.add(new TrackCircle(p(2740, 165)));
    this.trackCircles.add(new TrackCircle(p(2770, 180)));
    this.trackCircles.add(new TrackCircle(p(2800, 195)));
    this.trackCircles.add(new TrackCircle(p(2830, 210)));
    this.trackCircles.add(new TrackCircle(p(2860, 225)));
    this.trackCircles.add(new TrackCircle(p(2890, 240)));
    this.trackCircles.add(new TrackCircle(p(2920, 255)));
    this.trackCircles.add(new TrackCircle(p(2950, 270)));
    this.trackCircles.add(new TrackCircle(p(2980, 285)));
    this.trackCircles.add(new TrackCircle(p(3010, 300)));
    this.trackCircles.add(new TrackCircle(p(3040, 315)));

    this.trackLines
        .add(new TrackLine(p(3040, 315), p(3600, 800), 10, Track.cB));

    this.trackLines
        .add(new TrackLine(p(3900, 900), p(3840, 800), 10, Track.cB));
    this.trackLines
        .add(new TrackLine(p(3700, 800), p(3760, 700), 10, Track.cB));
    this.trackLines
        .add(new TrackLine(p(3900, 700), p(3840, 600), 10, Track.cB));
    this.trackLines
        .add(new TrackLine(p(3700, 600), p(3760, 500), 10, Track.cB));
    this.trackLines
        .add(new TrackLine(p(3900, 500), p(3840, 400), 10, Track.cB));
    this.trackLines
        .add(new TrackLine(p(3700, 400), p(3760, 300), 10, Track.cB));
    this.trackLines
        .add(new TrackLine(p(3900, 300), p(3840, 200), 10, Track.cB));
    this.trackLines
        .add(new TrackLine(p(3700, 200), p(3760, 100), 10, Track.cB));

    this.trackLines.add(new TrackLine(p(3760, 0), p(5000, 0), 10, Track.cB));

    this.endCheckpoint = new Checkpoint(p(4800, 50), Checkpoint.COLOR_ORANGE,
        null, null);

    this.recordOneLightning = TimeUnit.SECONDS.toMillis(15);
    this.recordTwoLightning = TimeUnit.SECONDS.toMillis(10);
    this.recordThreeLightning = TimeUnit.SECONDS.toMillis(8);
  }
}
