package com.jdroidev.rideagain.tracks;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import android.graphics.Point;
import android.hardware.SensorManager;

import com.badlogic.gdx.math.Vector2;
import com.jdroidev.rideagain.R;
import com.jdroidev.rideagain.game.Track;
import com.jdroidev.rideagain.game.trackelements.Checkpoint;
import com.jdroidev.rideagain.game.trackelements.TrackBox;
import com.jdroidev.rideagain.game.trackelements.TrackCircle;
import com.jdroidev.rideagain.game.trackelements.TrackDynamicPlatform;
import com.jdroidev.rideagain.game.trackelements.TrackEventPlatform;
import com.jdroidev.rideagain.game.trackelements.TrackLine;
import com.jdroidev.rideagain.game.trackelements.TrackLinesGroup;
import com.jdroidev.rideagain.game.trackelements.TrackPipe;

public class Track_5 extends Track {
  public Track_5() {
    load();
  }

  protected void load() {
    this.id = 5;
    this.nameResource = R.string.track_4;
    this.initialZoomFactor = 0.6f;
    this.initialGravity = new Vector2(0f, -SensorManager.GRAVITY_EARTH);
    this.bikeInitialSpawnPosition = p(100, 200);

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(0, 200));
        add(p(0, 0));
        add(p(200, 0));
        add(p(240, -20));
        add(p(280, -40));
        add(p(320, -80));
      }
    }, 10, Track.cB));

    this.trackPipes.add(new TrackPipe(p(320, -80), p(320, -1000),
        p(1600, -1000), 10, TrackPipe.DOWN_LEFT, 10, "#000000"));

    this.trackLines.add(new TrackLine(p(1600, -1000), p(2400, -1000), 10,
        "#000000"));

    this.trackPipes.add(new TrackPipe(p(2400, -1000), p(2700, -1000), p(2700,
        -700), 10, TrackPipe.DOWN_RIGHT, 10, "#000000"));

    this.trackPipes.add(new TrackPipe(p(2400, -500), p(2700, -500), p(2700,
        -700), 10, TrackPipe.UP_RIGHT, 10, "#000000"));

    this.trackLines.add(new TrackLine(p(2000, -600), p(2100, -700), 10,
        "#000000"));
    this.trackLines.add(new TrackLine(p(2100, -700), p(2200, -700), 10,
        "#000000"));

    this.trackEventPlatforms.add(new TrackEventPlatform(p(2200, -700), p(2600,
        -300), 10, Track.cB, p(2100, -670), TrackEventPlatform.SET_VISIBLE));

    this.trackEventPlatforms.add(new TrackEventPlatform(p(2600, -300), p(2800,
        -100), 10, Track.cB, p(2700, 270), TrackEventPlatform.SET_HIDDEN));

    this.trackLines.add(new TrackLine(p(2800, -100), p(2900, 100), 10,
        "#000000"));

    this.trackPipes.add(new TrackPipe(p(2500, 100), p(2500, 300), p(2700, 300),
        10, TrackPipe.UP_LEFT, 10, "#000000"));

    this.trackPipes.add(new TrackPipe(p(2700, 300), p(2900, 300), p(2900, 100),
        10, TrackPipe.UP_RIGHT, 10, "#000000"));

    this.trackLines.add(new TrackLine(p(2500, 100), p(2600, -100), 10,
        "#000000"));

    this.trackEventPlatforms.add(new TrackEventPlatform(p(2600, -100), p(3400,
        -500), 10, Track.cB, p(2700, 270), TrackEventPlatform.SET_VISIBLE));

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(3400, -500));
        add(p(4000, -500));
        add(p(4600, -600));
      }
    }, 10, Track.cB));

    this.checkpoints.add(new Checkpoint(p(3640, -450), Checkpoint.COLOR_BLUE,
        null, null));

    this.trackPipes.add(new TrackPipe(p(4600, -600), p(5000, -600), p(5000,
        -200), 10, TrackPipe.DOWN_RIGHT, 10, "#000000"));

    this.trackPipes.add(new TrackPipe(p(5010, -150), p(5010, -300), p(5300,
        -300), 10, TrackPipe.DOWN_LEFT, 10, "#000000"));

    this.trackLines
        .add(new TrackLine(p(5300, -300), p(6000, 0), 10, "#000000"));

    this.trackLines.add(new TrackLine(p(6300, 0), p(6800, 0), 10, "#000000"));

    this.trackEventPlatforms.add(new TrackEventPlatform(p(6800, 0), p(7200, 0),
        10, Track.cB, p(7150, 270), TrackEventPlatform.SET_HIDDEN));

    this.trackPipes.add(new TrackPipe(p(7200, 0), p(7350, 0), p(7350, 150), 10,
        TrackPipe.DOWN_RIGHT, 10, "#000000"));

    this.trackPipes.add(new TrackPipe(p(7150, 300), p(7350, 300), p(7350, 150),
        10, TrackPipe.UP_RIGHT, 10, "#000000"));

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(6600, -200));
        add(p(7400, -500));
        add(p(8200, -500));
      }
    }, 10, Track.cB));

    this.endCheckpoint = new Checkpoint(p(8000, -450), Checkpoint.COLOR_ORANGE,
        null, null);

    this.recordOneLightning = TimeUnit.SECONDS.toMillis(15);
    this.recordTwoLightning = TimeUnit.SECONDS.toMillis(10);
    this.recordThreeLightning = TimeUnit.SECONDS.toMillis(8);
  }
}
