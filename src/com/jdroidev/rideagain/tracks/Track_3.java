package com.jdroidev.rideagain.tracks;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import android.graphics.Point;
import android.hardware.SensorManager;

import com.badlogic.gdx.math.Vector2;
import com.jdroidev.rideagain.R;
import com.jdroidev.rideagain.game.Track;
import com.jdroidev.rideagain.game.trackelements.Checkpoint;
import com.jdroidev.rideagain.game.trackelements.TrackDynamicPlatform;
import com.jdroidev.rideagain.game.trackelements.TrackLine;
import com.jdroidev.rideagain.game.trackelements.TrackLinesGroup;

public class Track_3 extends Track {
  public Track_3() {
    load();
  }

  protected void load() {
    this.id = 3;
    this.nameResource = R.string.track_3;
    this.initialZoomFactor = 0.6f;
    this.initialGravity = new Vector2(0f, -SensorManager.GRAVITY_EARTH);
    this.bikeInitialSpawnPosition = p(100, 200);

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(0, 200));
        add(p(0, 0));
        add(p(300, 0));
        add(p(500, 100));
        add(p(700, 400));
        add(p(1000, 900));
      }
    }, 10, Track.cB));

    this.trackLines
        .add(new TrackLine(p(1030, 900), p(1200, 1200), 10, Track.cB));
    this.trackLines.add(new TrackLine(p(1220, 1200), p(1400, 1220), 10,
        Track.cB));
    this.trackLines.add(new TrackLine(p(1450, 1220), p(1700, 1500), 10,
        Track.cB));
    this.trackLines.add(new TrackLine(p(1800, 1540), p(2200, 1540), 10,
        Track.cB));

    this.trackDynamicPlatforms.add(new TrackDynamicPlatform(p(2800, 1530), 400,
        20, Track.cB, TrackDynamicPlatform.HORIZONTAL, 400, 400,
        TrackDynamicPlatform.MOVING_LEFT, 3));

    this.trackLines.add(new TrackLine(p(3400, 1540), p(4000, 1540), 10,
        Track.cB));

    this.checkpoints.add(new Checkpoint(p(3700, 1590), Checkpoint.COLOR_BLUE,
        null, null));

    this.trackDynamicPlatforms.add(new TrackDynamicPlatform(p(4600, 1530), 400,
        20, Track.cB, TrackDynamicPlatform.HORIZONTAL, 400, 400,
        TrackDynamicPlatform.MOVING_RIGHT, 3));

    this.trackDynamicPlatforms.add(new TrackDynamicPlatform(p(5800, 1530), 400,
        20, Track.cB, TrackDynamicPlatform.HORIZONTAL, 400, 400,
        TrackDynamicPlatform.MOVING_LEFT, 3));

    this.trackLines.add(new TrackLine(p(6400, 1540), p(7000, 1540), 10,
        Track.cB));

    this.trackLines.add(new TrackLine(p(7100, 1450), p(7700, 1000), 10,
        Track.cB));

    this.trackLines
        .add(new TrackLine(p(7800, 800), p(8000, 400), 10, Track.cB));

    this.trackLines
        .add(new TrackLine(p(8050, 300), p(8500, 250), 10, Track.cB));

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(8650, 200));
        add(p(9000, 150));
        add(p(9400, 100));
        add(p(9600, 0));
        add(p(10000, 0));
      }
    }, 10, Track.cB));

    this.endCheckpoint = new Checkpoint(p(9900, 50), Checkpoint.COLOR_ORANGE,
        null, null);

    this.recordOneLightning = TimeUnit.SECONDS.toMillis(15);
    this.recordTwoLightning = TimeUnit.SECONDS.toMillis(10);
    this.recordThreeLightning = TimeUnit.SECONDS.toMillis(8);
  }
}
