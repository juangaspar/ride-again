package com.jdroidev.rideagain.tracks;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import android.graphics.Point;
import android.hardware.SensorManager;

import com.badlogic.gdx.math.Vector2;
import com.jdroidev.rideagain.R;
import com.jdroidev.rideagain.game.Track;
import com.jdroidev.rideagain.game.trackelements.Checkpoint;
import com.jdroidev.rideagain.game.trackelements.TrackLine;
import com.jdroidev.rideagain.game.trackelements.TrackLinesGroup;
import com.jdroidev.rideagain.game.trackelements.TrackPipe;
import com.jdroidev.rideagain.game.trackelements.TrackZoomPoint;

public class Track_1 extends Track {
  public Track_1() {
    load();
  }

  protected void load() {
    this.id = 1;
    this.nameResource = R.string.track_1;
    this.initialZoomFactor = 0.7f;
    this.initialGravity = new Vector2(0f, -SensorManager.GRAVITY_EARTH);
    this.bikeInitialSpawnPosition = p(100, 200);

    this.checkpoints.add(new Checkpoint(p(2500, -1150), Checkpoint.COLOR_BLUE,
        null, null));
    this.checkpoints.add(new Checkpoint(p(4800, -250), Checkpoint.COLOR_BLUE,
        null, null));

    this.trackZoomPoints.add(new TrackZoomPoint(p(4800, -250), 0.6f));

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(0, 200));
        add(p(0, 0));
        add(p(400, 0));
        add(p(1000, -600));
        add(p(1200, -600));
        add(p(1400, -400));
        add(p(1600, -400));
        add(p(2400, -1200));
        add(p(2600, -1200));
        add(p(3000, -800));
        add(p(3400, -800));
        add(p(3800, -400));
        add(p(4000, -400));
        add(p(4200, -200));
        add(p(4400, -200));
        add(p(4400, -250));
        add(p(4600, -250));
        add(p(4600, -300));
        add(p(4800, -300));
      }
    }, 10, "#000000"));

    this.trackPipes.add(new TrackPipe(p(4800, -300), p(6400, -300),
        p(6400, 300), 10, TrackPipe.DOWN_RIGHT, 10, "#000000"));

    this.trackPipes.add(new TrackPipe(p(6450, 350), p(8000, 350),
        p(8000, -200), 10, TrackPipe.UP_RIGHT, 10, "#000000"));

    this.trackLines.add(new TrackLine(p(8000, -250), p(8400, -250), 10,
        "#000000"));

    this.endCheckpoint = new Checkpoint(p(8300, -200), Checkpoint.COLOR_ORANGE,
        null, null);

    this.recordOneLightning = TimeUnit.SECONDS.toMillis(15);
    this.recordTwoLightning = TimeUnit.SECONDS.toMillis(10);
    this.recordThreeLightning = TimeUnit.SECONDS.toMillis(8);
  }
}
