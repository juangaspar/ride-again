package com.jdroidev.rideagain.tracks;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import android.graphics.Point;
import android.hardware.SensorManager;

import com.badlogic.gdx.math.Vector2;
import com.jdroidev.rideagain.R;
import com.jdroidev.rideagain.game.Track;
import com.jdroidev.rideagain.game.trackelements.Checkpoint;
import com.jdroidev.rideagain.game.trackelements.TrackBox;
import com.jdroidev.rideagain.game.trackelements.TrackCircle;
import com.jdroidev.rideagain.game.trackelements.TrackLine;
import com.jdroidev.rideagain.game.trackelements.TrackLinesGroup;
import com.jdroidev.rideagain.game.trackelements.TrackTPlatform;

public class Track_2 extends Track {
  public Track_2() {
    load();
  }

  protected void load() {
    this.id = 2;
    this.nameResource = R.string.track_2;
    this.initialZoomFactor = 0.7f;
    this.initialGravity = new Vector2(0f, -SensorManager.GRAVITY_EARTH);
    this.bikeInitialSpawnPosition = p(100, 200);

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(0, 200));
        add(p(0, 0));
        add(p(230, 0));
      }
    }, 10, Track.cB));

    this.trackCircles.add(new TrackCircle(p(260, -10)));
    this.trackCircles.add(new TrackCircle(p(310, -10)));
    this.trackCircles.add(new TrackCircle(p(360, -10)));
    this.trackCircles.add(new TrackCircle(p(410, -10)));
    this.trackCircles.add(new TrackCircle(p(460, -10)));

    this.trackLines.add(new TrackLine(p(490, -5), p(1320, -200), 10, Track.cB));

    this.trackBoxes.add(new TrackBox(p(1380, -240), 70, 70, Track.cB));
    this.trackBoxes.add(new TrackBox(p(1480, -240), 70, 70, Track.cB));
    this.trackBoxes.add(new TrackBox(p(1580, -240), 70, 70, Track.cB));
    this.trackBoxes.add(new TrackBox(p(1680, -240), 70, 70, Track.cB));
    this.trackBoxes.add(new TrackBox(p(1780, -240), 70, 70, Track.cB));

    this.checkpoints.add(new Checkpoint(p(1880, -170), Checkpoint.COLOR_BLUE,
        null, null));

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(1840, -200));
        add(p(2640, -200));
        add(p(2840, 0));
        add(p(3240, 0));
        add(p(3240, 100));
        add(p(3440, 100));
      }
    }, 10, Track.cB));

    this.trackTPlatforms.add(new TrackTPlatform(p(3040, 0), 100, 380, 200, 0,
        10, 100, 0, Track.cB, Track.cB));

    this.checkpoints.add(new Checkpoint(p(3740, -170), Checkpoint.COLOR_BLUE,
        null, null));

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(3440, -200));
        add(p(3840, -200));
        add(p(4840, 300));
      }
    }, 10, Track.cB));

    this.trackLines
        .add(new TrackLine(p(5040, 100), p(5840, 600), 10, Track.cB));

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(6040, 500));
        add(p(6840, 1000));
        add(p(7440, 1000));
      }
    }, 10, Track.cB));

    this.endCheckpoint = new Checkpoint(p(7240, 1050), Checkpoint.COLOR_ORANGE,
        null, null);

    this.recordOneLightning = TimeUnit.SECONDS.toMillis(15);
    this.recordTwoLightning = TimeUnit.SECONDS.toMillis(10);
    this.recordThreeLightning = TimeUnit.SECONDS.toMillis(8);
  }
}
