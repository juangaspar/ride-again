package com.jdroidev.rideagain.tracks;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import android.graphics.Point;
import android.hardware.SensorManager;

import com.badlogic.gdx.math.Vector2;
import com.jdroidev.rideagain.R;
import com.jdroidev.rideagain.game.Track;
import com.jdroidev.rideagain.game.trackelements.Checkpoint;
import com.jdroidev.rideagain.game.trackelements.TrackBooster;
import com.jdroidev.rideagain.game.trackelements.TrackDynamicPlatform;
import com.jdroidev.rideagain.game.trackelements.TrackEventPlatform;
import com.jdroidev.rideagain.game.trackelements.TrackLine;
import com.jdroidev.rideagain.game.trackelements.TrackLinesGroup;
import com.jdroidev.rideagain.game.trackelements.TrackPipe;

public class Track_7 extends Track {
  public Track_7() {
    load();
  }

  protected void load() {
    this.id = 6;
    this.nameResource = R.string.track_4;
    this.initialZoomFactor = 0.5f;
    this.initialGravity = new Vector2(0f, -SensorManager.GRAVITY_EARTH);
    this.bikeInitialSpawnPosition = p(100, 1300);

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(0, 1200));
        add(p(0, 1000));
        add(p(140, 1000));
        add(p(1000, 0));
        add(p(1500, -600));
        add(p(2140, -1000));
      }
    }, 10, Track.cB));

    this.trackLines.add(new TrackLine(p(2140, -1000), p(2800, -1000), 10,
        "#000000"));

    this.trackPipes.add(new TrackPipe(p(2800, -1000), p(3400, -1000), p(3400,
        -400), 10, TrackPipe.DOWN_RIGHT, 10, "#000000"));

    this.trackBoosters.add(new TrackBooster(p(3370, -430), new Vector2(0f,
        SensorManager.GRAVITY_EARTH * 3)));

    this.trackEventPlatforms.add(new TrackEventPlatform(p(3000, 300), p(4000,
        300), 10, Track.cB, p(3400, 400), TrackEventPlatform.SET_VISIBLE));

    this.trackPipes.add(new TrackPipe(p(4000, 300), p(4300, 300), p(4300, 600),
        10, TrackPipe.DOWN_RIGHT, 10, "#000000"));

    this.trackLines
        .add(new TrackLine(p(4300, 600), p(5100, 800), 10, "#000000"));

    this.trackLines.add(new TrackLine(p(5150, 750), p(5600, 1400), 10,
        "#000000"));

    this.trackLines.add(new TrackLine(p(5600, 1400), p(5950, 1850), 10,
        "#000000"));
    this.trackLines.add(new TrackLine(p(5950, 1850), p(6400, 2100), 10,
        "#000000"));

    this.trackDynamicPlatforms.add(new TrackDynamicPlatform(p(7000, 2100), 400,
        20, Track.cB, TrackDynamicPlatform.HORIZONTAL, 400, 400,
        TrackDynamicPlatform.MOVING_LEFT, 6));

    this.checkpoints.add(new Checkpoint(p(7700, 2150), Checkpoint.COLOR_BLUE,
        null, null));

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(7600, 2100));
        add(p(8000, 2100));
        add(p(9000, 200));
        add(p(9200, 0));
        add(p(9800, 0));
      }
    }, 10, Track.cB));

    this.trackLinesGroups.add(new TrackLinesGroup(new ArrayList<Point>() {
      {
        add(p(8100, 2200));
        add(p(9100, 300));
        add(p(9300, 100));
      }
    }, 10, Track.cB));

    this.endCheckpoint = new Checkpoint(p(9600, 50), Checkpoint.COLOR_ORANGE,
        null, null);

    this.recordOneLightning = TimeUnit.SECONDS.toMillis(15);
    this.recordTwoLightning = TimeUnit.SECONDS.toMillis(10);
    this.recordThreeLightning = TimeUnit.SECONDS.toMillis(8);
  }
}
